package mathClass;

public class Random {
    public static void main(String[] args) {
      
            System.out.println(Math.random()); // generates random number from 0.0 to 1.0
            System.out.println(Math.random() * 10);//generates random number from 0.0 to 10.0
            System.out.println(Math.random() * 20);//generates random number from 0.0 to 20.0
            System.out.println(Math.random() * 11);//generates random number from 0.0 to 10.0 included
            System.out.println(Math.round(Math.random() * 10));//generates random number from 0.0 to 10.0 included and rounds it up to a whole number
            System.out.println((int)(Math.random() * 11));//generates random number from 0 to 10 where 10 is included
            System.out.println(Math.round(Math.random() * 10) + 10);//generates random number from 10 to 20 included and rounds it up to a whole number


        /*


            // Steps to produce a random number between 2 nrs: ( 17 and 53)
            1. biggest - smallest + 1
            2. (int) (Math.random() * 37) -> 0 and 36 (both inclusive)
            3. Add smallest nr to your result
            (int) (Math.random() * 37) + 17






         */








        }
}
