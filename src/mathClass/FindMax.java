package mathClass;

public class FindMax {
    public static void main(String[] args) {
        // finding the max of 2 numbers
        int num1 = 10;
        int num2 = 15;

        int max = Math.max(num1, num2);

        System.out.println(max);
        //finding the max of 4 numbers

        int number1 = 2;
        int number2 = 8;
        int number3 = 5;
        int number4 = 18;

        // max between number1 and number2 = 8
        // max between number3 and number4 = 18

        int max1 = Math.max(number1, number2);
        int max2 = Math.max(number3, number4);
        int finalMax = Math.max(max1, max2);

        System.out.println(finalMax);




    }
}
