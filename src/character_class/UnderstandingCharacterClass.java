package character_class;

import utilities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {

        //print true if str starts with uppercase, print false otherwise

        String str = ScannerHelper.getString();

        char firstChar = str.charAt(0);

        System.out.println(65 <= firstChar && firstChar <= 90); //instead of decimal values from ASCII

        //use Character class and useful methods
        System.out.println(Character.isUpperCase(firstChar)); //true
        System.out.println(Character.isLowerCase(firstChar)); //false

        System.out.println(Character.isLetter(firstChar));
        System.out.println(Character.isLetterOrDigit(firstChar));

        System.out.println(Character.isDigit(firstChar)); // is digit

        System.out.println(Character.isWhitespace(firstChar)); // is space
        System.out.println(Character.isSpaceChar(firstChar)); // is space
    }
}
