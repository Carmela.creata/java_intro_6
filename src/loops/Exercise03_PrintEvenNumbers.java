package loops;

public class Exercise03_PrintEvenNumbers {
    public static void main(String[] args) {

        // even numbers from 0 to 10

        for (int i = 0; i <= 10; i++) {
            if (i % 2 == 0) {
                System.out.println(i);
            }
        }

        //second way
        for (int i = 0; i <= 10 ; i= 10+2) {
            System.out.println(i);

        }

        //third way
        for (int i = 0; i <= 5 ; i++) {
            System.out.println(i * 2);

        }
    }
}
