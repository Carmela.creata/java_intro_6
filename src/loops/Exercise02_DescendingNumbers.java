package loops;

public class Exercise02_DescendingNumbers {
    public static void main(String[] args) {

        //descending numbers from 100 to 0

        for (int i = 100; i >= 0; i--){
            System.out.println(i);
        }
    }
}
