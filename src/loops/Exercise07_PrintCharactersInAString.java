package loops;

import utilities.ScannerHelper;

public class Exercise07_PrintCharactersInAString {
    public static void main(String[] args) {

        //ask user to enter a String
        //print each character of the String in a separate line
        //h
        //e
        //l
        //l
        //o
        
        String str = ScannerHelper.getString();

        for (int i = 0; i < str.length(); i++) {
            System.out.println(str.charAt(i));
            
        }
    }
}
