package loops;

public class Exercise11_FizzBuzz {
    public static void main(String[] args) {

        for (int i = 1; i <= 30; i++) {
            if(i % 3 == 0 && i % 5 == 0 ) System.out.println("FizzBuzz"); // you can put % 15 instead of &&(15 is divisible by 3 and 5
            else if (i % 5 == 0) System.out.println("Buzz");
            else if(i % 3 == 0 ) System.out.println("Fizz");
            else System.out.println(i);

        }


    }

}

