package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {

        //reverse a string

        String name = ScannerHelper.getName();

        String reverseName = "";

        for (int i = name.length() -1; i >= 0; i--) {
            reverseName += name.charAt(i);
        }
        System.out.println(reverseName);



        }


    }

