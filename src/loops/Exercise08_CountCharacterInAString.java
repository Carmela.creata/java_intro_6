package loops;

import utilities.ScannerHelper;

public class Exercise08_CountCharacterInAString {
    public static void main(String[] args) {

        String str = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.toUpperCase().contains("A")) count++;
        }  //close the loop and print separate
        System.out.println(count);


        }
    }

