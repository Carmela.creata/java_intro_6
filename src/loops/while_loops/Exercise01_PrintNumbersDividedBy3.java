package loops.while_loops;

public class Exercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {

        /*
        Write a prog that prints all the numbers divided by 3, starting from 1 to 100,both included
         */

        int num = 1;

        while (num <= 100){
            if(num % 3 == 0) System.out.println(num);
            num++;
        }
    }
}
