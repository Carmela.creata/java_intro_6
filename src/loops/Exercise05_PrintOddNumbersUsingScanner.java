package loops;


import utilities.ScannerHelper;

public class Exercise05_PrintOddNumbersUsingScanner {
    public static void main(String[] args) {

       int userNumber = ScannerHelper.getNumber();

        for (int i = 0; i <= userNumber ; i++) {
            if (i % 2 == 1)System.out.println(i);

        }


        }
    }

