package loops.practices;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {

             /*
             Write a Java program that asks user to enter a
    number
    if the number is more than or equals 10, then finish the
    program but if number is less than 10, keep asking user
    to enter a new number until user enters a number that
    is more than or equals 10.
    •Example program
    Program: Please enter a number
    User: 3
    Program: This number is not more than or equals 10

              */


        int num;
        int counter = 1;

        do {
            if(counter > 1) System.out.println("This number is not more than equal to 10");
            num = ScannerHelper.getNumber();

            counter++;
        }while (num < 10 );
        System.out.println("This number is more than equal to 10");



    }
    }

