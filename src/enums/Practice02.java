package enums;

public class Practice02 {
    public static void main(String[] args) {

        /*
       Write a program that prints wages as below
       hourly -> $20
       daily -> $160
       weekly -> $800
       bi-weekly -> $1600
       monthly -> $3200
       yearly -> $38400
       */
        Frequency frequency = Frequency.DAILY;
        switch(frequency){
            case DAILY:
                System.out.println("$160");
            case HOURLY:
                System.out.println("$20");
            case WEEKLY:
                System.out.println("$800");
            case BI_WEEKLY:
                System.out.println("$1600");
            case YEARLY:
                System.out.println("$38000");
            case MONTHLY:
                System.out.println("$3200");
        }



    }
}
