package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Practice06_ArrayList_Tasks {
    public static void main(String[] args) {
        System.out.println("Task1");
        System.out.println(Arrays.toString(getDoublenums(new int []{3, 2, 5, 7, 0})));

        System.out.println("Task2");
        System.out.println(secondMax(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));

        System.out.println("Task3");
        System.out.println(secondMin(new ArrayList<>(Arrays.asList(2, 3, 7, 1, 1, 7, 1))));

        System.out.println("Task4");
        System.out.println(removeEmpty(new ArrayList<>(Arrays.asList("Tech", "Global","", null, "School"))));

        System.out.println("Task5");
        System.out.println(remove3orMore(new ArrayList<>(Arrays.asList(200, 5, 100, 99, 101, 75, -5))));

        System.out.println("Task6");
        System.out.println(uniquesWords("Star Light Star Bright"));
    }


    /*
    Write a method called as double to double each element
    in an int array and return it back. NOTE: The return type is an array.
     Test data 1:
     {3, 2, 5, 7, 0}
    Expected output 1:
     [6, 4, 10, 14, 0]
     */

    public static int[] getDoublenums(int [] array){

        for (int i = 0; i < array.length; i++) {
            array[i] *= 2;

        }
        return array;
    }

    /*
    Write a method called as
secondMax
 to find and return
the second max number in an ArrayList
Test data 1:
{2, 3, 7, 1, 1, 7, 1}
Expected output 1:
3

     */
    public static int secondMax(ArrayList<Integer> list){


        Collections.sort(list);
        for (int i = list.size() -2; i >= 0 ; i--) {
            if(list.get(i) < list.get(list.size()- 1)) return list.get(i);

        }
        return 0;

    }
    //same but second min :)
    public static int secondMin(ArrayList<Integer> list){

        Collections.sort(list);
        for (int i = 1; i < list.size(); i++) {
            if(list.get(i) > list.get(0)) return list.get(i);

        }
        return 0;
    }

    /*
    Write a method called as
removeEmpty
to find and
remove all the elements in an ArrayList that are empty or
null.
Then, return the modified ArrayList back.
Test data 1:
["Tech", "Global", "", null, "", "School"]
Expected output 1:
["Tech", "Global", "School"]

     */
    public static ArrayList<String> removeEmpty(ArrayList<String>list){
        ArrayList<String> noEmpty = new ArrayList<>();
        list.removeIf(element -> element == null || element.isEmpty() );
        return list;

        //way2

       /* for (String s : list) {
            if(!s.isEmpty()) no.Empty.add(s) );
        }
        return noEmpty; */
    }
    /*
    Write a method called as
remove3orMore
 to find and remove all
the elements in an ArrayList that are more than 2 digits.
Then, return the modified ArrayList back.
NOTE: - sign should not counted as a digit when it is negative
number.
Test data 1:
[200, 5, 100, 99, 101, 75]
Expected output 1:
[5, 99, 75]

     */
    public static ArrayList<Integer> remove3orMore(ArrayList <Integer> list){
        list.removeIf(e -> Math.abs(e) >= 100 );
        return list;

       /*
       way2
        ArrayList<Integer> listRemovedElements = new ArrayList<>();
    for (int n : list) {
        if(Math.abs(n) < 100) listRemovedElements.add(n);
    }
    return listRemovedElements;
        */
    }

    /*
    Write a method called as
uniquesWords
 to find and return all the
unique words in a String.
NOTE: The return type is an ArrayList.
NOTE: Assume that you will not be given extra spaces.
Test data 1:
"TechGlobal School”
Expected output 1:
["TechGlobal", "School"]
Test data 2:
"Star Light Star Bright"
Expected output 2:
["Star", "Light", "Bright"]
     */

    public static ArrayList<String> uniquesWords(String str){
        ArrayList<String> strAsList = new ArrayList<>();

        String[] arr = str.split(" "); //[Star, Light, Star, Bright]

        for(String s : arr){
            if(!strAsList.contains(s)) strAsList.add(s);
        }

        return strAsList;
    }



}













