package arraylist_linkedlist_vector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class _08_ArrayList_To_Array {
    public static void main(String[] args) {
        ArrayList<String> languages = new ArrayList<>();

        languages.add("Java");
        languages.add("C#");
        languages.add("JS");
        languages.add("Ruby");

        System.out.println(languages);
        System.out.println(languages.size()); //4

        Object[] myArray = languages.toArray(); // convert ArrayList to Array - use Object
        System.out.println(Arrays.toString(myArray));
        System.out.println(myArray.length);


    }
}
