package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

public class Homework12 {
    public static void main(String[] args) {
        System.out.println("TASK1");
        System.out.println(noDigit("123Hello World149"));
        System.out.println("TASK2");
        System.out.println(noVowel("TechGlobal"));
        System.out.println("TASK3");
        System.out.println(sumOfDigits("John's age is 29"));
        System.out.println("TASK4");
        System.out.println(hasUpperCase("John's age is 29"));
        System.out.println("TASK5");
        System.out.println(middleInt(-1, 25, 10));
        System.out.println("TASK6");
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println("TASK7");
        System.out.println("TASK8");
        System.out.println(Arrays.toString(categorizeCharacters(Arrays.toString(new String[]{"abc123$#%"}))));

    }

    /*
    -Create a method called noDigit()
-This method will take one String argument and it will
return a new String with all digits removed from the
original String
Test Data 1: “”
Expected Result 1: “”
Test Data 2: “Java”
Expected Result 2: “Java”
Test Data 3: “123Hello”
Expected Result 3: “Hello”
     */
    public static String noDigit(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }
        String result = "";

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                result += c;
            }
        }

        return result;
    }

    /*
    Create a method called noVowel()
    -This method will take one String argument and it will
    return a new String with all vowels removed from the
    original String
    Test Data 1: “”
    Expected Result 1: “”
    Test Data 2: “xyz”
    Expected Result 2: “xyz”
    Test Data 3: “JAVA”
    Expected Result 3: “JV”
    Test Data 4: “125$”
    Expected Result 4: “125$”
    Test Data 5: “TechGlobal”
    Expected Result 5: “TchGlbl”
     */
    public static String noVowel(String str) {
        if (str == null || str.isEmpty()) {
            return str;
        }

        StringBuilder result = new StringBuilder();

        for (char c : str.toCharArray()) {
            if (!(Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e' || Character.toLowerCase(c) == 'o'
                    || Character.toLowerCase(c) == 'i' || Character.toLowerCase(c) == 'u'))
                result.append(c);
        }
        return result.toString();
       /*
       String result = input.replaceAll("[aeiouAEIOU]", "");

        return result;
        */

    }

    /*
      -Create a method called sumOfDigits()
  -This method will take one String argument and it will return an int sum
  of all digits from the original String.
  NOTE: Return zero if there is no digits in the String
  Test Data 1: “”
  Expected Result 1: 0
  Test Data 2: “Java”
  Expected Result 2: 0
  Test Data 3: “John’s age is 29”
  Expected Result 3: 11
  Test Data 4: “$125.0”
  Expected Result 4:
  */
    public static int sumOfDigits(String str) {

        if (str == null || str.isEmpty()) {
            return 0;
        }

        int sum = 0;

        // Iterate over each character in the input string
        for (char c : str.toCharArray()) {
            // Check if the character is a digit
            if (Character.isDigit(c)) {
                // Convert the character to an integer and add it to the sum
                sum += Character.getNumericValue(c);
            }
        }

        return sum;
        /*
        int sum = 0;
    //str = str.replaceAll("[^1-9]"
    //for (char c:str.toCharArray()){
    //    sum+= Integer.parseInt("" + c);
    //}
    //return sum;
         */
    }

    /*
    -Create a method called hasUpperCase()
-This method will take one String argument and it will return boolean
true if there is an uppercase letter and false otherwise.
Test Data 1: “”
Expected Result 1: false
Test Data 2: “java”
Expected Result 2: false
Test Data 3: “John’s age is 29”
Expected Result 3: true
Test Data 4: “$125.0”
Expected Result 4: false
*/
    public static boolean hasUpperCase(String str) {
        if (str == null || str.isEmpty())
            return false;

        boolean upperCase = false;

        for (char c : str.toCharArray()) {
            if (Character.isUpperCase(c))
                upperCase = true;
        }
        return upperCase;

    }

    /*
     -Create a method called middleInt()
 -This method will take three int arguments and it will return the middle
 int.
 Test Data 1: 1, 1, 1
 Expected Result 1: 1
      */
    public static int middleInt(int a, int b, int c) {

        if ((a <= b && b <= c) || (c <= b && b <= a)) {
            return b;
        } else if ((b <= a && a <= c) || (c <= a && a <= b)) {
            return a;
        } else {
            return c;
        }

    }

    /*
      Create a method called no13()
  -This method will take an int array as argument and it will return a new
  array with all 13 replaced with 0.
  NOTE: Assume length will always be more than zero.
  Test Data 1: [1, 2, 3 ,4]
  Expected Result 1: [1, 2, 3 ,4]
  Test Data 2: [13, 2, 3 ]
  Expected Result 2: [0, 2, 3 ]
  Test Data 3:[13, 13, 13 , 13, 13]
  Expected Result 3: [0, 0, 0, 0, 0]
    */
    public static int[] no13(int[] arr) {
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 13)
                result[i] = 0;
            else
                result[i] = arr[i];

        }
        return result;

    }

    /*
    Create a method called arrFactorial()
    -This method will take an int array as argument and it will return the
    array with every number replaced with their factorials.
    NOTE: If given array is empty, then return it back empty.
    NOTE: 0! equals to 1
    Test Data 1: [1, 2, 3, 4]
    Expected Result 1: [1, 2, 6, 24]
    Test Data 2: [0, 5]
    Expected Result 2: [1, 120]
    Test Data 3:[5 , 0, 6]
    Expected Result 3: [120, 1, 720]
    Test Data 4:[]

     */
    //factorial(3) = 3 * factorial(2) -> 3*2 -> 6
    //factorial(2) = 2 * factorial(1) -> 2*1 -> 2
    //factorial(1) = 1 * factorial(0) -> 1*1 -> 1
    //factorial(0) = 1
    public static int factorial(int x) {//3, 2, 1
        if (x == 0) return 1;
        return x * factorial(x - 1);
    }

    public static int[] arrFactorial(int[] arr) {//[1,2,3,4]
        //for (int i = 0; i < arr.length; i++) {
        // arr[i] = factorial(arr[i]);
        //   }

        for (int i = 0; i < arr.length; i++) {
            arr[i] = IntStream.rangeClosed(1, arr[i]).reduce(1, (x, y) -> x * y);
        }

        //for (int i = 0; i < arr.length; i++) {
        // int factorial = 1;//1, 2, 6, 24
        // for (int j = 1; j <= arr[i]; j++) {
        //    factorial *= j;
        // }
        // arr[i] = factorial;
        //}
        return arr;

    }


       /*
       Requirement:
       -Create a method called categorizeCharacters()
       -This method will take String as an argument and return a String array as
       letters at index of 0, digits at index of 1 and specials at index of 2.
       NOTE: IGNORE SPACES
       NOTE: Assume length will always be more than zero.
       Test Data 1: “     ”
       Expected Result 3: [ , , ]
       Test Data 2: “abc123$#%”
       Expected Result 2: [abc, 123, $#%]
       Test Data 3: “12ab$%3c%”
       Expected Result 3: [abc, 123, $%%]
       */

    public static String[] categorizeCharacters(String str) {
        return new String[]{str.replaceAll("[^a-zA-Z]", ""),
                str.replaceAll("[^0-9]", ""),
                str.replaceAll("[a-zA-Z0-9 ]", "")};


    }
}














