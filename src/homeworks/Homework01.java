package homeworks;

import java.sql.SQLOutput;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n......TASK 1......\n");
        /*
        JAVA =01001010010000010101011001000001
        SELENIUM =0101001101000101010011000100010101001101010010010101010101001101
        */

        System.out.println("\n......TASK 2......\n");
        /*
        77 M, 104 h, 121 y, 83 S, 108 l
         */

        System.out.println("\n......TASK 3......\n");

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself.\"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry");
        System.out.println("it.");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your ");
        System.out.println("life crossing them.");


        System.out.println("\n......TASK 4......\n");
        System.out.println("\tJava is easy to write and easy to run—this is the \nfoundational strength of Java and why many developers \nprogram in it. When you write Java once, you can run it \nalmost anywhere at any time." +
                        "\n\n\tJava can be used to create complete applications \n" +
                        "that can run on a single computer or be distributed \n" +
                        "across servers and clients in a network.\n\n\tAs a result, you can use it to easily build mobile \n" +
                        "applications or run-on desktop applications that use \n" +
                        "different operating systems and servers, such as Linux \n" +
                        "or Windows.");

        System.out.println("\n......TASK 5......\n");


        int myAge = 29;
        System.out.println(myAge);
        int myFavoriteNumber = 15;
        System.out.println(myFavoriteNumber);

        double myHeight = 5.01;
        System.out.println(myHeight);

        double myWeight = 110;
        System.out.println(myWeight);

        char myFavoriteLetter = 'C';
        System.out.println(myFavoriteLetter);

















        
    }
}
