package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n......TASK 1......\n");

        ArrayList<Integer> numbers = new ArrayList<>(Arrays.asList(10, 23, 67, 23, 78));

        System.out.println(numbers.get(3));
        System.out.println(numbers.get(0));
        System.out.println(numbers.get(2));

        System.out.println(numbers);


        System.out.println("\n......TASK 2......\n");

        ArrayList<String> colors = new ArrayList<>(Arrays.asList("Blue", "Brown", "Red", "White", "Black", "Purple"));
        System.out.println(colors.get(1));
        System.out.println(colors.get(3));
        System.out.println(colors.get(5));

        System.out.println(colors);


        System.out.println("\n......TASK 3......\n");

        ArrayList<Integer> numbs = new ArrayList<>(Arrays.asList(23, -34, -56, 0, 89, 100));

        System.out.println(numbs);
        Collections.sort(numbs);
        System.out.println(numbs);


        System.out.println("\n......TASK 4......\n");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(cities);
        Collections.sort(cities);
        System.out.println(cities);


        System.out.println("\n......TASK 5......\n");

        ArrayList<String> marvels = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panther", "Deadpool", "Captain America"));
        System.out.println(marvels);

        System.out.println(marvels.contains("Wolverine"));



        System.out.println("\n......TASK 6......\n");

        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));

        Collections.sort(avengers);
        System.out.println(avengers);

        System.out.println(avengers.contains("Hulk") && avengers.contains("Iron Man"));



        System.out.println("\n......TASK 7......\n");


        ArrayList<Character> characters = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));

        System.out.println(characters);
        System.out.println(characters.get(0));
        System.out.println(characters.get(1));
        System.out.println(characters.get(2));
        System.out.println(characters.get(3));
        System.out.println(characters.get(4));
        System.out.println(characters.get(5));
        System.out.println(characters.get(6));
        System.out.println(characters.get(7));
        System.out.println(characters.get(8));



        System.out.println("\n......TASK 8......\n");

        ArrayList<String> objects = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(objects);
        Collections.sort(objects);
        System.out.println(objects);

        int countM = 0;

        for(String object : objects){
            if(object.toUpperCase().startsWith("M"))
                countM ++;
        }
        System.out.println(countM);

        int countVowels = 0;

        for(String object : objects){
            if(object.toLowerCase().contains("a") || object.toLowerCase().contains("e"))
                countVowels++;
        }
        System.out.println(objects.size() - countVowels);


        System.out.println("\n......TASK 9......\n");

        ArrayList<String> utensils = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        System.out.println(utensils);

        int uppercase = 0, lowercase = 0, hasP = 0, startOrEndWithP = 0;

        for(String s : utensils){
            if(Character.isUpperCase(s.charAt(0))) uppercase++;
            else if(Character.isLowerCase(s.charAt(0))) lowercase++;
             if (s.toLowerCase().contains("p")) hasP++;
             if (s.toLowerCase().startsWith("p") || s.toLowerCase().endsWith("p")) startOrEndWithP++;
            else System.out.println(s);
        }

        System.out.println("Elements starts with uppercase = " + uppercase);
        System.out.println("Elements starts with lowercase = " + lowercase);
        System.out.println("Elements having P or p = " + hasP);
        System.out.println("Elements starting or ending with  P or p = " + startOrEndWithP);



        System.out.println("\n......TASK 10......\n");

        ArrayList<Integer> list = new  ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78 ));

        System.out.println(list);

        int count10 = 0, evenNum = 0, oddNum = 0, lessOrGreater = 0;

        for( Integer number : list){
            if(number % 10 == 0) count10++;
            if(number % 2 == 0 && number > 15) evenNum++;
            if(number % 2 == 1 && number < 20) oddNum++;
            if(number < 15 || number > 50) lessOrGreater++;
        }
        System.out.println("Elements that can be divided by 10 = " + count10);
        System.out.println("Elements that are even and greater than 15 = " + evenNum);
        System.out.println("Elements that are odd and less than 20 = " + oddNum);
        System.out.println("Elements that are less than 15 or greater than 50 = " + lessOrGreater);
















    }



}
