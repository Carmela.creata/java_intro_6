package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework09 {
    public static void main(String[] args) {
        System.out.println("==========Task01==========\n");

        int[] arr1 = {3, 4, 3, 3, 5, 5, 6, 6, 7};
        System.out.println(firstDuplicatedNumber(arr1));
        System.out.println("\n==========Task02==========\n");
        System.out.println("\n==========Task03==========\n");
        System.out.println("\n==========Task04==========\n");

        String[] arr2 = {"A", "foo"};
        System.out.println(duplicatedStrings(arr2));

        System.out.println("\n==========Task05==========\n");

        String[] arr3 = {"ABC", "foo", "XYZ"};
        reversedArray(arr3);
        System.out.println("\n==========Task06==========\n");

        System.out.println(reveredString("Java is fun"));



    }


        /*

    It should print “There is no duplicates” if there are no duplicate
    elements.
    NOTE: Make your code dynamic that works for any given int array.
    Test data 1:
    int[] numbers = {-4, 0, -7, 0, 5, 10, 45, 45};

    Expected output 1:
    0
    Test data 2:
    int[] numbers = {-8, 56, 7, 8, 65};

    Expected output 2:
    There is no duplicates
    Test data 3:
    int[] numbers = {3, 4, 3, 3, 5, 5, 6, 6, 7};

    Expected output 2:
         */
        public static int firstDuplicatedNumber(int[] arr){
            ArrayList<Integer> container = new ArrayList<>();

            for (int i : arr) {
                if(container.contains(i)){
                    return i;
                }else  container.add(i);
            }
            return -1;
        }


    /*
     Write a program to find the first duplicated String in a String array, ignore
     cases. It should print “There is no duplicates” if there are no duplicate
     elements.
     NOTE: Make your code dynamic that works for any given String array.
     Test data 1:
     String[] words = {“Z”, “abc”, “z”, “123”, “#” };

     Expected output 1:
     Z
     Test data 2:
     String[] words = {“xyz”, “java”, ”abc”};

     Expected output 2:
     There is no duplicates
     Test data 3:
     String[] words = {“a”, “b”, “B”, “XYZ”, “123”};

     Expected output 2:
     b
     */
    public static String firstDuplicatedString (String[] arr){
        ArrayList<String> container = new ArrayList<>();

        for (String s : arr) {
            if(container.contains(s.toLowerCase())){
                return s;
            }else  container.add(s.toLowerCase());
        }
        return "There is no duplicates";
    }

    /*
    Write a program to find the all duplicates in an int array.
     It should print “There is no duplicates” if there are no
     duplicate elements.
     NOTE: Make your code dynamic that works for any
     given int array.
     Test data 1:
     int[] numbers = {0, -4, -7, 0, 5, 10, 45, -7, 0};

     Expected output 1:
     0
     -7
     Test data 2:
     int[] numbers = {1, 2, 5, 0, 7};

     Expected output 2:
     There is no duplicates
     */
    public static ArrayList<Integer> duplicatedStrings(int[] arr){
        ArrayList<Integer> dupContainer = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i] == arr[j] && !dupContainer.contains(arr[i])) dupContainer.add(arr[i]);
            }
        }
        return dupContainer;
    }
    /*
    Requirement:
    Write a program to find the all duplicates in a String array,
    ignore cases. It should print “There is no duplicates” if
    there are no duplicate elements.
    NOTE: Make your code dynamic that works for any given
    String array.
    Test data 1:
    String[] words = {“A”, “foo”, “12” , “Foo”, “bar”, “a”, “a”,
    “java”};

    Expected output 1:
    A
    foo
    Test data 2:
    String[] words = {“python”, “foo”, “bar”, “java”, “123” };

    Expected output 2:
    There is no duplicate
     */

    public static ArrayList<String> duplicatedStrings(String[] arr){
        ArrayList<String> dupContainer = new ArrayList<>();
        ArrayList<String> solution = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if(arr[i].equalsIgnoreCase(arr[j]) && !dupContainer.contains(arr[i].toLowerCase())){

                    dupContainer.add(arr[i].toLowerCase());
                    solution.add(arr[i]);

                }
            }
        }
        return solution;
    }
        /*
        Write a method called as reversedArray to reverse
    elements in an array, then print array.
    NOTE: Make your code dynamic that works for any
    given array.
    Test data 1:
    String[] words1 = {“abc”, “foo”, “bar”};

    Expected output 1:
    [bar, foo, abc]
    Test data 2:
    String[] words2 = {“java”, “python”, “ruby”};

    Expected output 2:
    [ruby, python, java
         */
        public static void reversedArray(String[] arr){
        /*
        WAY: 1 -> Max Iteration
        ArrayList<String> reversedList = new ArrayList<>();
        for (int i = arr.length-1; i >= 0 ; i--) {
            reversedList.add(arr[i]);
        }
        System.out.println(reversedList);
         */

        /*
        Second way:
        arr = a, b, c, d, e, f
        i: 0
        startingIndex: 0 1 2 3
        endingIndex: 5 4 3 2
        Storage: a, b, c
        first i: f, b, c, d, e, a
        sec i:   f, e, c, d, b, a
        third i: f, e, d, c, b, a
        int startingIndex = 0;
        int endingIndex = arr.length-1;
        String storage;
        while(startingIndex < endingIndex){
            storage = arr[startingIndex];
            arr[startingIndex] = arr[endingIndex];
            arr[endingIndex] = storage;
            startingIndex++;
            endingIndex--;
        }
        System.out.println(Arrays.toString(arr));
         */

            //Way 3:
            Collections.reverse(Arrays.asList(arr));
            System.out.println(Arrays.toString(arr));

        }

        /*
        Write a method called as reverseStringWords to reverse
    each word in a given String
    NOTE: Make your code dynamic that works for any
    given String.
    Test data 1:
    String str = “Java is fun”;

    Expected output 1:
    avaJ si nuf
    Test data 2:
    String str = “Today is a fun day”;

    Expected output 2:
    yadoT si a nuf yad
         */
        public static String reveredString(String str){
            String reveredStr = "";

            String[] strAsArr = str.trim().split("\\s+");

            for (String s : strAsArr) {
                reveredStr += new StringBuffer(s).reverse() + " ";

            }

            return reveredStr.trim();

        }



}
