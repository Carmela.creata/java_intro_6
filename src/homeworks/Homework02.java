package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {


        System.out.println("\n......TASK 1......\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Enter number 5");
        int num1 = input.nextInt();
        System.out.println("The number 1 entered by user = " + num1);

        System.out.println("Enter number 10");
        int num2 = input.nextInt();
        System.out.println("The number 2 entered by user = " + num2);


        System.out.println("The sum of number 1 and number 2 entered by user is = " + (num1 + num2));
        String name = input.nextLine();


        System.out.println("\n......TASK 2......\n");


        System.out.println("Enter number 3");
        int number1 = input.nextInt();
        System.out.println("The number 1 entered by user = " + number1);

        System.out.println("Enter number 5");
        int number2 = input.nextInt();
        System.out.println("The number 2 entered by user = " + number2);
        System.out.println("The product of the given 2 numbers is: " + number1 * number2);


        System.out.println("\n......TASK 3......\n");

        System.out.println("Enter number 24");
        double d1 = input.nextDouble();

        System.out.println("The number 1 entered by user = " + d1);

        System.out.println("Enter number 10");
        double d2 = input.nextDouble();

        System.out.println("The number 2 entered by user = " + d2);


        System.out.println("The sum of the given numbers is = " + (d1 + d2));
        System.out.println("The product of the given numbers is = " + d1 * d2);
        System.out.println("The subtraction of the given numbers is = " + (d1 - d2));
        System.out.println("The division of the given numbers is = " + d1 / d2);
        System.out.println("The remainder of the given numbers is = " + d1 % d2);


        System.out.println("\n......TASK 4......\n");

        System.out.println("1.\t\t" + (-10 + 7 * 5));
        System.out.println("2.\t\t" + (72 + 24) % 24);
        System.out.println("3.\t\t" + (10 + -3 * 9 / 9));
        System.out.println("4. \t\t" + (5 + 18 / 3 * 3 - (6 % 3)));







        System.out.println("\n......TASK 5......\n");

        System.out.println("Please enter number 7");
        int a1 = input.nextInt();

        System.out.println("Please enter number 11");
        int a2 = input.nextInt();

        int result = (a1 + a2) / 2;


        System.out.println("The average of the given numbers is: " + result);


        System.out.println("\n......TASK 6......\n");

        System.out.println("Please enter number 6");
        int n1 = input.nextInt();

        System.out.println("Please enter number 10");
        int n2 = input.nextInt();

        System.out.println("Please enter number 12");
        int n3 = input.nextInt();

        System.out.println("Please enter number 15");
        int n4 = input.nextInt();

        System.out.println("Please enter number 17");
        int n5 = input.nextInt();

        int average = (n1 + n2 + n3 + n4 + n5) / 5;
        System.out.println("Please enter the average of the given numbers:" + average);


        System.out.println("\n......TASK 7......\n");

        System.out.println("Please enter number 5");
        int d = input.nextInt();

        System.out.println("Please enter number 6");
        int e = input.nextInt();

        System.out.println("Please enter number 10");
        int f = input.nextInt();

        System.out.println("The 5 multiplied with 5 is = " + d *d);
        System.out.println("The 5 multiplied with 5 is = " + e * e);
        System.out.println("The 5 multiplied with 5 is = " + f * f);

        System.out.println("\n......TASK 8......\n");

        System.out.println("Please enter the side of the square ");
        int s = input.nextInt();
        int area = s * s;
        int perimeter = 4 *s;

        System.out.println("Perimeter of the square is:" + perimeter);
        System.out.println("Area of the square is:" + area);

        System.out.println("\n......TASK 9......\n");

        double averageSalary = 90_000;

        System.out.println("A Software Engineer in Test can earn $" + averageSalary * 3 + " in three years.");


        System.out.println("\n......TASK 10......\n");

         String favBook, favColor;
         int favNumber;

        System.out.println("What is you favorite book?");
        favBook = input.next();


        System.out.println("What is you favorite color?");
        favColor = input.next();

        System.out.println("What is you favorite number?");
         favNumber = input.nextInt();
         input.nextLine();


        System.out.println("User's favorite book is:" + favBook + "\nUser's favorite color is:" + favColor +
                 "\nUser's favorite number is:" + favNumber);

        System.out.println("\n......TASK 11......\n");

        String firstName, lastName, emailAddress, phoneNumber, address;
        int age;

        System.out.println("Please enter your first name");
        firstName = input.nextLine();

        System.out.println("Please enter your last name");
        lastName = input.nextLine();

        System.out.println("Please enter your age");
        age = input.nextInt();
        input.nextLine();

        System.out.println("Please enter your email address");
        emailAddress = input.nextLine();

        System.out.println("Please enter your phone number");
        phoneNumber = input.nextLine();


        System.out.println("Please enter your address");
        address = input.nextLine();

        System.out.println("\t\tUser who joined this program is " + firstName + " " +  lastName + ". " + firstName + "' age is \n" +
                 age + ". " + firstName + "'s email address is " + emailAddress + ", phone number \nis " + phoneNumber +
                 ", and address is " + address + ".");








        








































































    }
}
