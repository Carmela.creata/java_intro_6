package homeworks;

import java.util.*;

public class Homework15 {
    public static void main(String[] args) {
        System.out.println("TASK1");
        System.out.println(Arrays.toString(fibonacciSeries1(3)));
        System.out.println("TASK2");
        System.out.println(fibonacciSeries2(4));
        System.out.println("TASK3");
        int[] arr1 = {1, 2, 3, 4};
        int[] arr2 = {3, 4, 5, 5};
        int[] result3 = findUniques(arr1, arr2);
        System.out.println(Arrays.toString(result3));
        System.out.println("TASK4");
        int[] arr3 = {1, 2, 2, 3};
        System.out.println(firstDuplicate(arr3));
        System.out.println("TASK5");
        System.out.println(isPowerOf3(3));

    }
    /*
    -Create a method called fibonacciSeries1()
     -This method will take an int argument as n, and it will
     return n series of Fibonacci numbers as an int
     array.
     REMEMBER: Fibonacci series = 0, 1, 1, 2, 3, 5, 8, 13, 21
     Test Data 1: 3
     Expected Result 1: [0, 1, 1]
     Test Data 2: 5
     Expected Result 2: [0, 1, 1, 2, 3]
     */

    public static int[] fibonacciSeries1(int n){
        int[] result = new int[n];
        int first = 0;
        int second = 1;

        for (int i = 0; i < n ; i++) {
            if(i <= 1) result[i]  = i;
            else   {
                int fib = first + second;
                result[i] = fib;
                first = second;
                second = fib;
                
            }
        }
        return result;
    }

    /*
    Create a method called fibonacciSeries2()
    -This method will take an int argument as n, and it will
    return the nth series of Fibonacci number as an
    int.
    REMEMBER: Fibonacci series = 0, 1, 1, 2, 3, 5, 8, 13, 21
    Test Data 1: 2
    Expected Result 1: 1
    Test Data 2: 4
    Expected Result 2: 2
     */

    public static int fibonacciSeries2(int n){
        if(n < 0) return 0;
        if(n == 1 || n == 2) return 1;

        int fib = 0;
        int first = 1;
        int second = 1;

        for (int i = 3; i <= n; i++) {
            fib =  first + second;
            first = second;
            second = fib;
        }
        return fib;

    }

    /*
    Create a method called findUniques()
    -This method will take 2 int array argument and it will
    return an int array which has only the unique values from
    both given arrays.
    NOTE: If both arrays are empty, then return an empty array.
    NOTE: if one of the array is empty, then return unique values
    from the other array.
    Test Data 1: [], []
    Expected Result 1: []
    Test Data 2: [], [1, 2, 3, 2]
    Expected Result 2: [1, 2, 3]
    Test Data 3: [1, 2, 3, 4], [3, 4, 5, 5]
    Expected Result 3: [1, 2, 5]
     */
    public static int[] findUniques(int[] arr1, int[] arr2) {
        List<Integer> uniqueValues = new ArrayList<>();

        for (int num : arr1) {
            if (!uniqueValues.contains(num)) {
                uniqueValues.add(num);
            }
        }

        for (int num : arr2) {
            if (!uniqueValues.contains(num)) {
                uniqueValues.add(num);
            }
        }

        int[] result = new int[uniqueValues.size()];
        for (int i = 0; i < uniqueValues.size(); i++) {
            result[i] = uniqueValues.get(i);
        }

        return result;
    }
    /*
    Create a method called firstDuplicate()
    -This method will take an int array argument and it will
    return an int which is the first duplicated number.
    NOTE: All elements will be positive numbers.
    NOTE: If there are no duplicates, then return -1
    NOTE: If there are more than one duplicate, then return
    the one for which second occurrence has the smallest
    index.
    Test Data 1: []
    Expected Result 1: -1
    Test Data 2: [1]
    Expected Result 2: -1
    Test Data 3: [1, 2, 2, 3]
    Expected Result 3: 2
     */

    public static int firstDuplicate(int[] arr) {
        Set<Integer> uniques = new HashSet<>();

        for (int num:
            arr ) {
            if(uniques.contains(num)) {
                return num;
            }
            uniques.add(num);
        }
        return -1;
    }
    /*
    Create a method called isPowerOf3()
    -This method will take an int argument and it will
    return true if given int argument is equal to 3
    power of the X. Otherwise, it will return false.
    Numbers that are power of 3 = 1, 3, 9, 27, 81, 243....
    Test Data 1: 1
    Expected Result 1: true
    Test Data 2: 2
    Expected Result 2: false
    Test Data 3: 3
    Expected Result 3: true
    Test Data 4: 81
    Expected Result 4: true
     */
    public static boolean isPowerOf3(int num) {
        if (num <= 0) {
            return false;
        }

        while (num % 3 == 0) {
            num /= 3;
        }

        return num == 1;
    }
}

