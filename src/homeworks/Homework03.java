package homeworks;

import java.util.Random;
import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {

        System.out.println("\n......TASK 1......\n");

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter 2 numbers:");
        int num1 = input.nextInt();
        int num2 = input.nextInt();
        input.nextLine();

        System.out.println(Math.abs(num1 - num2));
        System.out.println("The difference between numbers is = " + (Math.abs(num1 - num2)));


        System.out.println("\n......TASK 2......\n");


        System.out.println("Please enter 5 numbers");
        int number1 = input.nextInt();
        int number2 = input.nextInt();
        int number3 = input.nextInt();
        int number4 = input.nextInt();
        int number5 = input.nextInt();

        int max1 = Math.max(number1, number2);
        int max2 = Math.max(number3, number4);
        int max3 = Math.max(max1, max2);
        int finalMax = Math.max(max3, number5);

        System.out.println("Max value = " + finalMax);

        int min1 = Math.min(number1, number2);
        int min2 = Math.min(number3, number4);
        int min3 = Math.min(min1, min2);
        int finalMin = Math.min(min3, number5);

        System.out.println("Min value = " + finalMax);


        System.out.println("\n......TASK 3......\n");

        Random myRandom = new Random();

        int random1 = myRandom.nextInt(51) + 50;
        int random2 = myRandom.nextInt(51) + 50;
        int random3 = myRandom.nextInt(51) + 50;


        System.out.println("Number 1 is = " + random1);
        System.out.println("Number 2 is = " + random2);
        System.out.println("Number 3 is = " + random3);

        int sum = random1 + random2 + random3;

        System.out.println("The sum of numbers is " + sum);


        System.out.println("\n......TASK 4......\n");

        double moneyGiven = 25.5;

        System.out.println("Alex's money: $" + (125 - moneyGiven));
        System.out.println("Mike's money: $" + (220 - moneyGiven));


        System.out.println("\n......TASK 5......\n");

        double pricePerDay = 15.60;
        double totalPrice = 390;
        System.out.println((int) (totalPrice / pricePerDay));


        System.out.println("\n......TASK 6......\n");

        String s1 = "5", s2 = "10";
        int numb1 = Integer.parseInt(s1);
        int numb2 = Integer.parseInt(s2);

        System.out.println("Sum of 5 and 10 is = " + (numb1 + numb2));
        System.out.println("Product of 5 and 10 is = " + (numb1 * numb2));
        System.out.println("Division of 5 and 10 is = " + (numb1 / numb2));
        System.out.println("Subtraction of 5 and 10 is = " + (numb1 - numb2));
        System.out.println("Remainder of 5 and 10 is = " + (numb1 % numb2));


        System.out.println("\n......TASK 7......\n");

        String a1 = "200", a2 = "-50";
        int nr1 = Integer.parseInt(a1);
        int nr2 = Integer.parseInt(a2);


        System.out.println("The greatest value is =  " + Math.max(nr1, nr2));
        System.out.println("The smallest value is =  " + Math.min(nr1, nr2));
        System.out.println("The average is =  " + (nr1 + nr2) / 2);
        System.out.println("The absolute difference is =  " + Math.abs(nr1 - nr2));


        System.out.println("\n......TASK 8......\n");


        double coins = 0.96;

        int d1 = 24, d2 = 168, d3 = 150;
        System.out.println((int) (d1 / coins) + " days");
        System.out.println((int) (d2 / coins) + " days");
        System.out.println("$" + coins * d3);


        System.out.println("\n......TASK 9......\n");


        double dailySavings = 62.5;
        double price = 1250;

        System.out.println((int) (price / dailySavings));


        System.out.println("\n......TASK 10......\n");

        double carPrice = 14265;
        double month1 = 475.50;
        double month2 = 951;

        System.out.println("Option 1 will take " + (int) (carPrice / month1) + " months");
        System.out.println("Option 2 will take " + (int) (carPrice / month2) + " months");


        System.out.println("\n......TASK 11......\n");


        System.out.println("Please enter 2 numbers");
        int no1 = input.nextInt();
        int no2 = input.nextInt();

        System.out.println((double) no1 / no2);


        System.out.println("\n......TASK 12......\n");

        int x = myRandom.nextInt(101);
        int y = myRandom.nextInt(101);
        int z = myRandom.nextInt(101);


        System.out.println("Number 1 " + x);
        System.out.println("Number 2 " + y);
        System.out.println("Number 3 " + z);

        System.out.println(x > 25 && y > 25 && z > 25);


        System.out.println("\n......TASK 13......\n");


        int n = myRandom.nextInt(8) + 1;

        System.out.println(n);

        if (n == 1) System.out.println("Monday");
        else if (n == 2) System.out.println("Tuesday");
        else if (n == 3) System.out.println("Wednesday");
        else if (n == 4) System.out.println("Thursday");
        else if (n == 5) System.out.println("Friday");
        else if (n == 6) System.out.println("Saturday");
        else if (n == 7) System.out.println("Sunday");


        System.out.println("\n......TASK 14......\n");

        System.out.println("Please tell me your exam results");
        int result1 = input.nextInt();
        int result2 = input.nextInt();
        int result3 = input.nextInt();

        int averageResults = (result1 + result2 + result3) / 3;

        if (averageResults >= 70) {
            System.out.println("YOU PASSED!");
        } else {
            System.out.println("YOU FAILED!");
        }


        System.out.println("\n......TASK 15......\n");


        System.out.println("Please enter 3 numbers");
        int e = input.nextInt();
        int f = input.nextInt();
        int g = input.nextInt();

        if (e != f && e != g && f != g) System.out.println("NO MATCH");
        else if (e == f && e == g && f == g) System.out.println("TRIPLE MATCH");
        else if (e == f && f == g) System.out.println("DOUBLE MATCH");

        // I used triple match second because the system would have not read triple match after
        //it already read double









    }

    }













































