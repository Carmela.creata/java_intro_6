package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {

        System.out.println("\n......TASK 1......\n");


        for (int i = 1; i <= 100; i++) {
            if (i % 7 == 0) {
                System.out.print(i);
                if (i != 98) {
                    System.out.print(" - ");
                }
            }
        }


        System.out.println("\n......TASK 2......\n");


        for (int i = 1; i <= 50; i++) {
            if (i % 2 == 0 && i % 3 == 0) {
                System.out.print(i);
                if (i != 48) {
                    System.out.print(" - ");
                }
            }

        }


        System.out.println("\n......TASK 3......\n");


        for (int i = 100; i >= 50; i--) {
            if (i % 5 == 0) {
                System.out.print(i);
                if (i != 50) {
                    System.out.print(" - ");
                }

            }

        }


        System.out.println("\n......TASK 4......\n");

        for (int i = 0; i <= 7; i++) {
            System.out.println("The square root of " + i + " is = " + i * i);

        }


        System.out.println("\n......TASK 5......\n");


        int sum = 0;

        for (int i = 1; i <= 10; i++) {
            sum += i;
        }
        System.out.println(sum);


        System.out.println("\n......TASK 6......\n");


        int num = ScannerHelper.getNumber();

        int fact = 1;

        for (int i = 1; i <= num; i++) {
            fact *= i;
        }
        System.out.println(fact);


        System.out.println("\n......TASK 7......\n");

        String name = ScannerHelper.getFullName().toLowerCase();

        int nameVowelCount = 0;

        for (int i = 0; i < name.length(); i++) {
            if (name.charAt(i) == 'a' || name.charAt(i) == 'e' || name.charAt(i) == 'i' || name.charAt(i) == 'o' || name.charAt(i) == 'u') {
                nameVowelCount++;
            }

        }
        System.out.println("There are " + nameVowelCount + " vowel letters in this full name");


        System.out.println("\n......TASK 8......\n");

        String name2;

        do {
            name2 = ScannerHelper.getName(); //ask user for a name

        } while(!name2.toLowerCase().startsWith("j")); // if it is true


        System.out.println("End of program");








    }
}