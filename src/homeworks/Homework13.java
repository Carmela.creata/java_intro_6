package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

public class Homework13 {
    public static void main(String[] args) {
        System.out.println("TASK1");
        System.out.println(hasLowerCase("hello"));
        System.out.println("TASK2");
        System.out.println(noZero(new ArrayList<>(Arrays.asList(1, 0, 10))));
        System.out.println("TASK3");
        System.out.println(Arrays.deepToString(numberAndSquare(new int[]{1, 2, 3})));
        System.out.println("TASK4");
        //not sure how to print this
        System.out.println("TASK5");
        System.out.println(reverseSentence("Java is fun"));
        System.out.println("TASK6");
        System.out.println(removeStringSpecialsDigits("Selenium 123#$%Cypress"));
        System.out.println("TASK7");
        System.out.println(Arrays.toString(removeArraySpecialsDigits(new String[]{"123abc", "#$%abc", "abc"})));
        System.out.println("TASK8");
        ArrayList<String> list1 = new ArrayList<>(Arrays.asList("Java", "Java", "fun"));
        ArrayList<String> list2 = new ArrayList<>(Arrays.asList("Java", "C#", "Python"));
        System.out.println(removeAndReturnCommons(list1, list2));
        System.out.println("TASK9");
        System.out.println(noXInVariables(new ArrayList<>(Arrays.asList("hello", "x", "hi", "xxxyyy", "x"))));



    }
    /*
    Create a method called hasLowerCase()
    -This method will take a String argument, and it will
    return boolean true if there is lowercase letter and false
    if it doesn’t.
    Test Data 1: “”
    Expected Result 1: false
    Test Data 2: “JAVA”
    Expected Result 2: false
    Test Data 3: “125$”
    Expected Result 3: false
    Test Data 4: “hello”
    Expected Result 4: true
     */

    public static boolean hasLowerCase(String str){
        boolean hasLowerCase = false;
        if (str.isEmpty()) return false;
        for (char c: str.toCharArray()
             ) {
            if(Character.isLowerCase(c))
                hasLowerCase = true;
        }
        return false;
    }

    /*
    Create a method called noZero()
    -This method will take one Integer ArrayList argument
    and it will return an ArrayList with all zeros removed
    from the original Integer ArrayList.
    NOTE: Assume that ArrayList size will be at least 1.
    Test Data 1: [1]
    Expected Result 1: [1]
    Test Data 2: [1, 1, 10]
    Expected Result 2: [1, 1, 10]
    Test Data 3: [0, 1, 10]
    Expected Result 3: [1, 10]
    Test Data 4: [0, 0, 0]
    Expected Result 4: []
     */
    public static ArrayList<Integer> noZero(ArrayList<Integer> list) {
        ArrayList<Integer> result = new ArrayList<>();

        for (int num : list)
            if (num != 0) {
                result.add(num);
            }
        return result;
       }

       /*
       Create a method called numberAndSquare()
    -This method will take an int array argument and it will
    return a multidimensional array with all numbers
    squared.
    NOTE: Assume that array size is at least 1.
    Test Data 1: [1, 2, 3]
    Expected Result 1: [[1, 1], [2, 4], [3, 9]]
    Test Data 2: [0, 3, 6]
    Expected Result 2: [[0, 0], [3, 9], [6, 36]]
    Test Data 3: [1, 4]
    Expected Result 3: [[1,1], [4, 16]]
        */
    public static int[][] numberAndSquare(int[] arr){
        int[][] result = new int[arr.length][2];//[{1,1},{2, 4},{3,9}]
        int index = 0;
        for (int i : arr){
            result[index][0] = i;
            result[index][1] = i*i;
        }
        return result;
    }
    /*
    Create a method called containsValue()
    -This method will take a String array and a String
    argument, and it will return a boolean. Search the
    variable inside of the array and return true if it exists in
    the array. If it doesn’t exist, return false.
    NOTE: Assume that array size is at least 1.
    NOTE: The method is case-sensitive
    Test Data 1: [“abc”, “foo”, “java”], “hello”
    Expected Result 1: false
    Test Data 2: [“abc”, “def”, “123”], “Abc”
    Expected Result 2: false
    Test Data 3: [“abc”, “def”, “123”, “Java”, “Hello”], “123”
    Expected Result 3: true
     */
    public static boolean containsValue(String[] arr, String str){
        int index = Arrays.binarySearch(arr,str);
        return index >= 0;
    }
    /*
    Create a method called reverseSentence()
    -This method will take a String argument and it will
    return a String with changing the place of every word.
    All words should be in reverse order. Make sure that there
    are two words inside the sentence at least. If there is no
    two words return “There is not enough words!”.
    NOTE: After you reverse, only first letter must be
    uppercase and the rest will be lowercase!
    Test Data 1: “Hello”
    Expected Result 1: “There is not enough words!”
    Test Data 2: “Java is fun”
    Expected Result 2: “Fun is java”
    Test Data 3: “This is a sentence”
    Expected Result 3: “Sentence a is this”
     */
    public static String reverseSentence(String str){
        str = str.toLowerCase();
        String[] strArr = str.trim().split("''+");
        String reversedStr = "";

        if(strArr.length < 2){
            return "There is not enough words!";
        }else{
            for (int i = strArr.length - 1; i >= 0 ; i--) {
                reversedStr += strArr[i] + " ";
            }
            reversedStr = reversedStr.trim();
            return reversedStr.substring(0,1).toUpperCase() + reversedStr.substring(1);
        }
    }

    /*
    Create a method called removeStringSpecialsDigits()
    -This method will take a String as argument, and it will
    return a String without the special characters or digits.
    NOTE: Assume that String length is at least 1.
    NOTE: Do not remove spaces.
    Test Data 1: “123Java #$%is fun”
    Expected Result 1: “Java is fun”
    Test Data 2: “Selenium”
    Expected Result 2: “Selenium”
    Test Data 3: “Selenium 123#$%Cypress”
    Expected Result 3: “Selenium Cypress”
     */
    public static String removeStringSpecialsDigits(String str){
       StringBuilder result = new StringBuilder();
       for(char c: str.toCharArray()){
           if(Character.isLetter(c) || c == ' ')
               result.append(c);
       }
       return result.toString();
    }

    /*
    -Create a method called removeArraySpecialsDigits()
    -This method will take a String array as argument, and
    it will return a String array without the special
    characters or digits from the elements.
    NOTE: Assume that array size is at least 1.
    Test Data 1: [“123Java”, “#$%is”, “fun”]
    Expected Result 1: [“Java”, “is”, “fun”]
    Test Data 2: [“Selenium”, “123$%”, “###”]
    Expected Result 2: [“Selenium”, “”, “”]
    Test Data 3: [“Selenium”, “123#$%Cypress”]
    Expected Result 3: [“Selenium”, “Cypress”
     */
    public static String[] removeArraySpecialsDigits(String[] arr){
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i].replaceAll("[^A-Za-z]", "");
        }
        return arr;
    }
    /*
    -Create a method called removeAndReturnCommons()
    -This method will take two String ArrayList and it will
    return all the common words as String ArrayList.
    NOTE: Assume that both ArrayList sizes are at least 1.
    Test Data 1: [“Java”, “is”, “fun”], [“abc”, “xyz”, “123”]
    Expected Result 1: []
    Test Data 2: [“Java”, “is”, “fun”], [“Java”, “C#”,
    “Python”]
    Expected Result 2: [“Java”]
    Test Data 3: [“Java”, “C#”, “C#”], [“Python”, “C#”, “C+
    +”]
    Expected Result 3: [“C#”]
     */
    public static ArrayList<String> removeAndReturnCommons(ArrayList<String> list1, ArrayList<String> list2){
        LinkedHashSet<String> set1 = new LinkedHashSet<>(list1);

        ArrayList<String> answer = new ArrayList<>();
        for (String s : set1) {
            if(list2.contains(s))
                answer.add(s);
        }
        return answer;
    }
    /*
    -Create a method called noXInVariables()
    -This method will take an ArrayList argument, and it will
    return an ArrayList with all the x or X removed from
    elements.
    If the element itself equals to x or X or contains only x
    letters, then remove that element.
    NOTE: Assume that ArrayList size is at least 1.
    Test Data 1: [abc, 123, #$%]
    Expected Result 1: [abc, 123, #$%]
    Test Data 2: [xyz, 123, #$%]
    Expected Result 2: [yz, 123, #$%]
    Test Data 3: [x, 123, #$%]
    Expected Result 3: [123, #$%]
    Test Data 4: [xyXyxy, Xx, ABC]
    Expected Result 4: [yyy, ABC
     */
    public static ArrayList<String> noXInVariables(ArrayList<String> list){//["hello", x, hi, xxxyyy, x] - [hello, hi, yyy]
        for (int i = 0; i < list.size(); i++) {
            list.set(i, list.get(i).replaceAll("[xX]", ""));
            if(list.get(i).isEmpty()){
                list.remove(i);
                i--;
            }
        }
        return list;
    }


 }


