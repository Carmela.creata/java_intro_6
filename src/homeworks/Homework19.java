package homeworks;

import java.util.Arrays;

public class Homework19 { public static void main(String[] args) {
    // Task 1
    int[] arr1 = {1, 5, 10};
    boolean bool1 = true;
    int sum1 = sum(arr1, bool1);
    System.out.println(sum1);  // Output: 11

    int[] arr2 = {3, 7, 2, 5, 10};
    boolean bool2 = false;
    int sum2 = sum(arr2, bool2);
    System.out.println(sum2);  // Output: 12

    // Task 2
    String str1 = "Java";
    int n1 = 2;
    String result1 = nthChars(str1, n1);
    System.out.println(result1);  // Output: "aa"

    String str2 = "JavaScript";
    int n2 = 5;
    String result2 = nthChars(str2, n2);
    System.out.println(result2);  // Output: "St"

    String str3 = "Java";
    int n3 = 3;
    String result3 = nthChars(str3, n3);
    System.out.println(result3);  // Output: "v"

    String str4 = "Hi";
    int n4 = 4;
    String result4 = nthChars(str4, n4);
    System.out.println(result4);  // Output: ""

    // Task 3
    String str5 = "Hello";
    String str6 = "Hi";
    boolean result5 = canFormString(str5, str6);
    System.out.println(result5);  // Output: false

    String str7 = "halogen";
    String str8 = "hello";
    boolean result6 = canFormString(str7, str8);
    System.out.println(result6);  // Output: false

    String str9 = "programming";
    String str10 = "gaming";
    boolean result7 = canFormString(str9, str10);
    System.out.println(result7);  // Output: true

    String str11 = "CONVERSATION";
    String str12 = "voices rant on";
    boolean result8 = canFormString(str11, str12);
    System.out.println(result8);  // Output: true

    // Task 4
    String str13 = "Apple";
    String str14 = "Peach";
    boolean result9 = isAnagram(str13, str14);
    System.out.println(result9);  // Output: false

    String str15 = "listen";
    String str16 = "silent";
    boolean result10 = isAnagram(str15, str16);
    System.out.println(result10);  // Output: true

    String str17 = "astronomer";
    String str18 = "moon starer";
    boolean result11 = isAnagram(str17, str18);
    System.out.println(result11);  // Output: true

    String str19 = "CINEMA";
    String str20 = "iceman";
    boolean result12 = isAnagram(str19, str20);
    System.out.println(result12);  // Output: true
}

    public static int sum(int[] arr, boolean isEvenIndexes) {
        int sum = 0;
        int startIndex = isEvenIndexes ? 0 : 1;
        for (int i = startIndex; i < arr.length; i += 2) {
            sum += arr[i];
        }
        return sum;
    }

    public static String nthChars(String str, int n) {
        if (str.length() < n) {
            return "";
        }
        StringBuilder result = new StringBuilder();
        for (int i = n - 1; i < str.length(); i += n) {
            result.append(str.charAt(i));
        }
        return result.toString();
    }

    public static boolean canFormString(String str1, String str2) {
        str1 = str1.toLowerCase().replaceAll("\\s", "");
        str2 = str2.toLowerCase().replaceAll("\\s", "");
        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);
        return Arrays.equals(charArray1, charArray2);
    }

    public static boolean isAnagram(String str1, String str2) {
        str1 = str1.toLowerCase().replaceAll("\\s", "");
        str2 = str2.toLowerCase().replaceAll("\\s", "");
        char[] charArray1 = str1.toCharArray();
        char[] charArray2 = str2.toCharArray();
        Arrays.sort(charArray1);
        Arrays.sort(charArray2);
        return Arrays.equals(charArray1, charArray2);
    }
}

