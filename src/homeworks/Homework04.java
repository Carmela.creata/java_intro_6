package homeworks;

import utilities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {
        

        System.out.println("\n......TASK 1......\n");

        String name = ScannerHelper.getName();

        int lengthOfName = name.length();

        System.out.println("The length of the name is = " + lengthOfName);
        System.out.println("The first character in the name is = " + name.charAt(0));
        System.out.println("The last character in the name is = " + name.substring(name.length() - 1));
        System.out.println("The first 3 characters in the name are = " + name.substring(0, 3));;
        System.out.println("The last 3 characters in the name are = " + name.substring(name.length() - 3));

        System.out.println(name.startsWith("A") || name.startsWith("a"));

        if(name.startsWith("A")){
            System.out.println("You are in the club");
        }
        else{
            System.out.println("Sorry, you are not in the club!");
        }


        System.out.println("\n......TASK 2......\n");

        String address = ScannerHelper.getAddress();


        if(address.contains("Chicago")){
            System.out.println("You are in the club!");
        }
        else if (address.contains("Des Plaines")){
            System.out.println("You are welcome to join the club!");
        }
        else {
            System.out.println("Sorry, you will never be in the club!");
        }

         


        System.out.println("\n......TASK 3......\n");

        String favCountry = ScannerHelper.getFavCountry();
        System.out.println(favCountry);
        
       favCountry = favCountry.toLowerCase();

      if(favCountry.contains("a") && favCountry.contains("i")){
          System.out.println("A and i are there");
        }
      else if(favCountry.contains("i")){
          System.out.println("I is there");
      }
      else if(favCountry.contains("a")){
          System.out.println("A is there");
      }
      else{
          System.out.println("A and i are not there");
      }


        System.out.println("\n......TASK 4......\n");

        String str = " Java is FUN ";
        str = str.replace(" Java is FUN ", "java is fun");
                System.out.println(str);


        System.out.println("The first word in the str is = " + str.substring(0,4));

        System.out.println("The second word in the str is = " + str.substring(5,7));

        System.out.println("The third word in the str is = " + str.substring(8));




















    }
}
