package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n......TASK 1......\n");

        int[] arr = new int[10];

        arr[2]  = 23;
        arr[4] = 12;
        arr[7] = 34;
        arr[9] = 7;
        arr[6] = 15;
        arr[0] = 89;

        System.out.println(arr[3]);
        System.out.println(arr[0]);
        System.out.println(arr[9]);


        System.out.println(Arrays.toString(arr));


        System.out.println("\n......TASK 2......\n");

        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]);
        System.out.println(str[0]);
        System.out.println(str[4]);

        System.out.println(Arrays.toString(str));


        System.out.println("\n......TASK 3......\n");

        int[] numbers =  {23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers));

        Arrays.sort(numbers);

        for (int i = 0; i < numbers.length; i++) {
            System.out.print(numbers[i] + ", ");

        }


        System.out.println("\n......TASK 4......\n");

        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};
        System.out.println(Arrays.toString(countries));

        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));


        System.out.println("\n......TASK 5......\n");

        String[] cartoons = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoons));

        boolean pluto = true;

        for(String cartoon : cartoons){
            if(cartoon.contains("Pluto")) System.out.println(true);
        }


        System.out.println("\n......TASK 6......\n");

        String[] cats = {"Garfield", "Tom", "Sylvester", "Azrael"};

        Arrays.sort(cats);
        System.out.println(Arrays.toString(cats));

        boolean containsGarfield = false;
        boolean containsFelix = false;

        for(String cat : cats) {
            System.out.println(cat.contains("Garfield") && cat.contains("Felix"));

        }

        System.out.println("\n......TASK 7......\n");

        double[] num = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(num));

        System.out.println(num[0]);
        System.out.println(num[1]);
        System.out.println(num[2]);
        System.out.println(num[3]);
        System.out.println(num[4]);


        System.out.println("\n......TASK 8......\n");

        char[] chars = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(chars));

        int letter = 0, uppercase = 0, lowercase = 0, digits = 0, specials = 0;

        for (int i = 0; i < chars.length; i++) {
            char c = chars[i];

            if(Character.isUpperCase(c)) {
                uppercase++;
                letter++;
            }
            else if(Character.isLowerCase(c)){
                lowercase++;
                letter++;
            }

            else if(Character.isDigit(c)){
                digits++;
            }
            else
                specials++;

        }
        System.out.println("Letters = " + letter);
        System.out.println("Uppercase letters = " + uppercase);
        System.out.println("Lowercase letters = " + lowercase);
        System.out.println("Digits = " + digits);
        System.out.println("Special characters = " + specials);


        System.out.println("\n......TASK 9......\n");

        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(objects));



        int upper = 0, lower = 0, bpcount = 0, bookpenCount = 0;
        for(String object : objects){
            if(Character.isUpperCase(object.charAt(0))){
                upper++;
            }
            else if(Character.isLowerCase(object.charAt(0))){
                lower++;
            }
            else if(object.toLowerCase().startsWith("b") || object.toLowerCase().startsWith("p")){
                bpcount++;
            }
            else if(object.toLowerCase().contains("book") || object.toLowerCase().contains("pen")) {
                bookpenCount++;
            }
            else System.out.println(object);

        }

        System.out.println("Elements starts with uppercase = " + upper);
        System.out.println("Elements starts with lowercase = " + lower);
        System.out.println("Elements starting with B or P = " + bpcount);
        System.out.println("Elements having book or pen = " + bookpenCount);


        System.out.println("\n......TASK 10......\n");

        int[] arrays = {3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        System.out.println(Arrays.toString(arrays));

        int moreThan10 = 0, lessThan10 = 0, are10 = 0;

        for (int array : arrays){
           if(array > 10){
               moreThan10++;
           } else if (array < 10) {
               lessThan10++;
           }
           else are10++;

           }

        System.out.println("Elements are more than 10 = " + moreThan10);
        System.out.println("Elements are less than 10 = " + lessThan10);
        System.out.println("Elements that are 10 = " + are10);


        System.out.println("\n......TASK 11......\n");

        int[] num1 = {5, 8, 13 ,1, 2};
        int[] num2 = {9, 3, 67, 1, 0};

        System.out.println(Arrays.toString(num1));
        System.out.println(Arrays.toString(num2));

        int[] num3 = new int[num1.length];

        for (int i = 0; i < num1.length; i++) {
            num3[i] = Math.max(num1[i], num2[i]);

        }
        System.out.println(Arrays.toString(num3));

















    }
}
