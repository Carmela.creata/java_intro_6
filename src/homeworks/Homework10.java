package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {
    public static void main(String[] args) {
        System.out.println("==========TASK8==========");
        int[] arr = {10, -13,12, 8, 15, -20};
        System.out.println(findClosestTo10(arr));

        System.out.println("==========TASK7==========");
        int[] arr2 = {10, 3, 6, 3, 2};
        int[] arr3 = {6, 8, 3, 0, 0, 7, 5, 10, 34};
        System.out.println(Arrays.toString(sum(arr2, arr3)));

        System.out.println("==========TASK6==========");
        System.out.println(removeExtraSpaces("  I   am      learning     Java     "));// Java is Fun

        System.out.println("==========TASK5==========");
        ArrayList<String> strList1 = new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"));

        System.out.println(removeDuplicateElements(strList1));// [java, C#, ruby, JAVA, C++]


        System.out.println("==========TASK4==========");
        ArrayList<Integer> intList1 = new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60));
        System.out.println(removeDuplicateNumbers(intList1));// [10, 20, 35, 60, 70]


        System.out.println("==========TASK02==========");
        System.out.println(countA("TechGlobal is a QA bootcamp"));





    }

    /*

    Write a method countWords() that takes a String as an
    argument, and returns how many words there are in the
    the given String
    Test data 1:
    String str = “      Java is fun       ”;
    Expected output 1:
    3
    Test data 2:
    String str = “Selenium is the most common UI
    automation tool.   ”;
    Expected output 2:
    8

    public static int countWords(String str) {
        int count = 1;
        for (int i = 0; i < str.length() - 1; i++) {
            if (str.charAt(i) == ' ' && str.charAt(i + 1) != ' ') count++;

        }
        return count++;

     */
    public static int countWords(String str){
        return str.trim().split("[\\s]+").length;
    }






    /*
     Write a method countA() that takes a String as an
     argument, and returns how many A or a there are in the the
     given String
     Test data 1:
     String str = “TechGlobal is a QA bootcamp”;
     Expected output 1:
     4
     Test data 2:
     String str = “QA stands for Quality Assurance”;
     Expected output 2:
     5

    public static int countA(String str) {
        int countA = 0;
        for (char c : str.toCharArray()) {
            if (Character.toLowerCase(c) == 'a') countA++;

        }
        return countA++;

     */
         public static int countA(String str){
        return str.replaceAll("[^aA]", "").length();
    }







    /*
        Write a method countPos() that takes an ArrayList of
    Integer as an argument, and returns how many
    elements are positive
    Test data 1:
    [-45, 0, 0, 34, 5, 67]
    Expected output 1:
    3
    Test data 2:
    [-23, -4, 0, 2, 5, 90, 123]
    Expected output 2:
    4

    public static int countPos(ArrayList<Integer> list) {
        int count = 0;
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i) > 0) {
                count++;
            }
        }
        return count;

    }
    */
    public static int countPos(ArrayList<Integer> list){
        return (int) list.stream().filter(e -> e > 0).count();
    }



    /*
    Write a method removeDuplicateNumbers() that takes
    an ArrayList of Integer as an argument, and returns it
    back with removed duplicates
    Test data 1:
    [10, 20, 35, 20, 35, 60, 70, 60]
    Expected output 1:
    [10, 20, 35, 60, 70]
    Test data 2:
    [1, 2, 5, 2, 3]
    Expected output 2:
    [1, 2, 5, 3]
     */
    public static ArrayList<Integer> removeDuplicateNumbers (ArrayList<Integer> list){
        ArrayList<Integer> newList = new ArrayList<>();

        for (int s : list) {
            if(!newList.contains(s)) newList.add(s);
        }
        return newList;
    }
    /*
    Write a method removeDuplicateElements() that takes an
    ArrayList of String as an argument, and returns it back with
    removed duplicates
    Test data 1:
    [“java”, “C#”, “ruby”, “JAVA”, “ruby”, “C#”, “C++”]
    Expected output 1:
    [“java”, “C#”, “ruby”, “JAVA”,  “C++”]
    Test data 2:
    [“abc”, “xyz”, “123”, “ab”, “abc”, “ABC”]
    Expected output 2:
    [“abc”, “xyz”, “123”, “ab”, “ABC”]
     */

    public static ArrayList<String> removeDuplicateElements (ArrayList<String> list){
        ArrayList<String> newList = new ArrayList<>();

        for (String s : list) {
            if(!newList.contains(s)) newList.add(s);
        }
        return newList;
    }
    /*
    Write a method removeExtraSpaces() that takes a String as
    an argument, and returns a String with removed extra spaces
    Test data 1:
    String str = “   I   am      learning     Java      ”;
    Expected output 1:
    I am learning Java
    Test data 2:
    String str = “Java  is fun    ”;
    Expected output 2:
    Java is fun
     */
    public static String removeExtraSpaces (String str){// java   is fun - > java is fun
        return str.trim().replaceAll("[\\s]+", " ");

//        String newStr = "";
//
//        char[] strAsArr = str.toCharArray();
//
//        for (int i = 0; i < strAsArr.length; i++) {
//            if(strAsArr[i] != ' ') newStr += strAsArr[i];
//            else if(i != strAsArr.length-1 && strAsArr[i+1] != ' ') newStr += strAsArr[i];
//        }
//
//        return newStr.trim();

    }

    /*
    Write a method add() that takes 2 int[] arrays as arguments and
    returns a new array with sum of given arrays elements.
    Test data 1:
    int[] arr1 = {3, 0, 0, 7, 5, 10};
    int[] arr2 = {6, 3, 2};
    Expected output 1:
    [9, 3, 2, 7, 5, 10]
    Test data 1:
    int[] arr1 =  {10, 3, 6, 3, 2};
    int[] arr2 = {6, 8, 3, 0, 0, 7, 5, 10, 34};
    Expected output 1:
    [16, 11, 9,  3, 2, 7, 5, 10, 34]

     */
    public static int[] sum (int[] arr1, int[] arr2){
        for (int i = 0; i < Math.min(arr1.length, arr2.length); i++) {
            if(arr1.length> arr2.length) arr1[i] += arr2[i];
            else arr2[i] += arr1[i];
        }

        return arr1.length > arr2.length ? arr1 : arr2;
    }

    /*
    Write a method findClosestTo10() that takes an int[] array as
    an argument, and returns the closest element to 10 from given
    array
    Test data 1:
    int[] numbers = {10, -13, 5, 70, 15, 57};
    Expected output 1:
    5
    Test data 2:
    int[] numbers = {10, -13, 8, 12, 15, -20};
    Expected output 2:
    8
     */
    public static int findClosestTo10 (int[] arr){// 3 5 6 10 12 25 45 62
//        Arrays.sort(arr);
//        int numberBefore10 = 0;
//        int numberAfter10 = 0;
//
//        for (int i = 0; i < arr.length; i++) {
//            if(arr[i] < 10) numberBefore10 = arr[i];
//            else break;
//        }
//        for (int i = arr.length - 1; i >= 0; i--) {
//            if(arr[i] > 10 ) numberAfter10 = arr[i];
//        }
//
//        if(Math.abs(numberBefore10 - 10) <= numberAfter10 - 10) return numberBefore10;
//        else return numberAfter10;


        Arrays.sort(arr);
        int closest = Integer.MAX_VALUE;
        for (int number : arr) {
            if (number != 10 && Math.abs(number - 10) < Math.abs(closest - 10)) {
                closest = number;
            }
        }
        return closest;

    }

}

