package homeworks;

public class Homework20 {public static void main(String[] args) {
    // Test Data 1
    int[] arr1 = {1, 5, 10};
    int[] arr2 = {3, 7, 2, 5, 10};
    System.out.println(sum(arr1, true)); // Output: 1
    System.out.println(sum(arr2, false)); // Output: 3

    // Test Data 2
    System.out.println(sumDigitsDouble("Java")); // Output: -1
    System.out.println(sumDigitsDouble("ab12")); // Output: 6
    System.out.println(sumDigitsDouble("23abc45")); // Output: 28
    System.out.println(sumDigitsDouble("Hi-23")); // Output: 10

    // Test Data 3
    System.out.println(countOccurrence("Hello", "World")); // Output: 0
    System.out.println(countOccurrence("Hello", "l")); // Output: 2
    System.out.println(countOccurrence("Can I can a can", "anc")); // Output: 3
    System.out.println(countOccurrence("IT conversations", "IT")); // Output: 1
}

    // Task-1
    public static int sum(int[] arr, boolean isEven) {
        int count = 0;
        for (int num : arr) {
            if (isEven && num % 2 == 0) {
                count++;
            } else if (!isEven && num % 2 != 0) {
                count++;
            }
        }
        return count;
    }

    // Task-2
    public static int sumDigitsDouble(String input) {
        int sum = 0;
        boolean hasDigits = false;

        for (char c : input.toCharArray()) {
            if (Character.isDigit(c)) {
                int digit = Character.getNumericValue(c);
                if (digit >= 0) {
                    sum += digit;
                    hasDigits = true;
                }
            }
        }

        return hasDigits ? sum * 2 : -1;
    }

    // Task-3
    public static int countOccurrence(String str1, String str2) {
        // Convert both strings to lowercase and remove whitespace for case-insensitivity
        str1 = str1.toLowerCase().replaceAll("\\s", "");
        str2 = str2.toLowerCase().replaceAll("\\s", "");

        int count = 0;
        int index = 0;

        while (index != -1) {
            index = str1.indexOf(str2, index);
            if (index != -1) {
                count++;
                index += str2.length();
            }
        }

        return count;
    }
}
