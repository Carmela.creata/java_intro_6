package homeworks;

public class Homework14 {
    public static void main(String[] args) {
        System.out.println("TASK1");
        fizzBuzz1(3);
        System.out.println("TASK2");
        System.out.println(fizzBuzz2(1));
        System.out.println("TASK3");
        System.out.println(findSumNumbers("a1b4c 6#"));
        System.out.println("TASK4");

        System.out.println("TASK5");
        System.out.println(countSequenceOfCharacters("abc"));


    }
    /*
     -Create a method called fizzBuzz1()
     -This method will take an int argument, and it will print
     numbers starting from 1 to given argument. BUT, it will print
     “Fizz” for the numbers divided by 3, “Buzz” for the numbers
     divided by 5, and “FizzBuzz” for the numbers divided both by
     3 and 5.
     */
    public static void fizzBuzz1(int num){
        for (int i = 1; i <= num; i++) {
            if(i % 15 == 0)
                System.out.println("FizzBuzz");
            else if (i % 3 == 0) {
                System.out.println("Fizz");
            } else if (i % 5 == 0) {
                System.out.println("Buzz");
            }
            else System.out.println(i);
        }
    }
    /*
    Create a method called fizzBuzz2()
    -This method will take an int argument, and it will return
    a String. BUT it will return “Fizz” if the given number is
    divided by 3, “Buzz” if the number is divided by 5, and
    “FizzBuzz” if the number is divided both by 3 and 5.
    Otherwise, it will return number itself.
     */

    public static String fizzBuzz2(int num){

       StringBuilder result = new StringBuilder();
        for (int i = 1; i <= num; i++) {
            if(i % 15 == 0)
                result.append(i);
            else if (i % 3 == 0) {
                result.append(i);
            } else if (i % 5 == 0) {
               result.append(i);
            }
            else result.append(i);
        }
        return result.toString();
    }
    /*
    Create a method called findSumNumbers()
    -This method will take a String argument and it will return
    an int which is the sum of all numbers presented in the
    String.
    NOTE: If there are no numbers represented in the String,
    return 0.
     */
    public static int findSumNumbers(String str){

        int sum = 0;
        String [] numbs = str.split("\\D+");
        for(String num : numbs){
            if(!num.isEmpty())
                sum += Integer.parseInt(num);
        }
        return sum;
    }
       /*
    Create a method called findBiggestNumber()
    -This method will take a String argument and it will
    return an int which is the number presented in the
    String.
         */
    

    /*
    Create a method called
    countSequenceOfCharacters()
    -This method will take a String argument and it will
    return a String which is the count of repeated
    characters in a sequence in the String.
    NOTE: If given String is empty, then return empty String.
    NOTE: It is case sensitive!!!
     */
    public static String countSequenceOfCharacters(String str){
        String answer = "";
        char letter = str.charAt(0);
        int count = 1;

        for (int i = 1; i < str.length(); i++) {
            if(str.charAt(i) == letter) count ++;
            else{
                answer += Integer.toString(count) + letter;
                letter = str.charAt(i);
                count = 1;
            }
        }
        return answer;

    }
}
