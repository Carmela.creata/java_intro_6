package gitDemo;

public class Demo {
    public static void main(String[] args) {

        String firstName = "Carmela ";
        String lastName = "Creata";

        System.out.println(firstName + lastName);

        String favColour = "red";
        System.out.println("My favorite color is " + favColour);

        /*
        I want you to print out My favorite car is " " and the model year is " "
         */
        String favCar = "Porsche";
        int year = 2023;
        System.out.println("My favorite car is " + favCar + " and the model year is " + year);
    }

}
