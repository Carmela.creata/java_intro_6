package casting;

public class Exercise01 {
    public static void main(String[] args) {

        int num1 = 5, num2 = 2;

        System.out.println(num1 / num2);

        System.out.println((double) num1 / num2); // 5.0/2 -> 2.5 - convert one or both numbers in double

        //Exercise 02

        double price = 900;
        double dailySaveAmount = 50;

        System.out.println(price / dailySaveAmount);
        System.out.println("You can buy the phone after " + (int) (price / dailySaveAmount) + " days.");



















    }
}
