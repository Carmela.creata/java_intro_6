package conditional_statements;

import java.util.Scanner;

public class Exercise06_CheckAll10 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        int x = (int) (Math.random() * 2 + 10);
        int y = (int) (Math.random() * 2 + 10);

        System.out.println(x);
        System.out.println(y);

        System.out.println(x == 10 && y == 10);
    }
}
