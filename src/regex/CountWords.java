package regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountWords {
    public static void main(String[] args) {
        /*
        Ask the user to enter a sentence and count how many words are in
the sentence.
The regex pattern for a single word is [A-Za-z]{1,}

print out how many words are in the sentence.
Scenario 1:
Program: Please enter a sentence
User: Hello, my name is john doe.
Program:
Hello
my
name
is
john
doe
This sentence contains 6 words
Scenario 2:
Program: Please enter a username
User:
Program: This sentence contains 0 words

         */
        String sentence = "hello,  my name is john.";
        String[] arr = sentence.split(" ");// ["hello,", "", "my", "name", "is", "john."]

        System.out.println("This sentence contains " + arr.length + " words");
        for (String s : arr) {
            System.out.println(s);
        }

        System.out.println("\n==========REGEX WAY===========\n");
        //REGEX WAY

        Pattern pattern = Pattern.compile("[a-zA-Z]{1,}");
        Matcher matcher = pattern.matcher("hello,  my name is john.");
        int wordCount = 0;

        while(matcher.find()){
            System.out.println(matcher.group());
            wordCount++;
        }
        System.out.println("This sentence contains " + wordCount + " words");



    }
}
