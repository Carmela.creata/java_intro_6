package variables;

public class UnderstandingVariables {public static void main(String[] args) {
    String name = "John"; // declaring and initializing the variable

    int age = 50; // declaring the variable without a value
    System.out.println(age);

    age = 50; // initializing the variable


    // capital letters and lowercase letters are seen differently with java
    double money = 5.5;
    double m0ney = 4.5;
    double Money;

    double d1 = 10; // declaring and initializing variable
    System.out.println(d1);

    d1 = 15.5; // reassigning d1 to 15.5
    System.out.println(d1);







}
}
