package operators.increments_decrement_operator;

public class PostIncrementPostDecrement {
    public static void main(String[] args) {
        int num1 = 10, num2 = 10;

        System.out.println(num1++); // 10 -> Increase next time
        System.out.println(num1);  // 11  -> Now it will be increased

        System.out.println(++num2); // 11 -> if it is in the left it will increase it right now
        System.out.println(num2); //11


        System.out.println("\n.....Task2......\n");

        int n1 = 5, n2 = 7;

        n1++; // keep it at 5 but next time increase it by 1
        n1 += n2;

        System.out.println(n1); // 13

        System.out.println("\n.....Task3......\n");

        int i1 = 10;
        --i1; // decrease it by 1 now -> 9;
        i1--; // decrease it by 1 for next use -> 7;
        System.out.println(--i1);

        System.out.println("\n.....Task4......\n");

        int number1 = 50;
        number1 -= 25; // 50-25 = 25;
        number1 -= 10; // 25-10 = 15
        System.out.println(number1--); //-> stays the same because it will decrease by 1 next time
        System.out.println(number1); // 14 HERE

        System.out.println("\n.....Task5......\n");

        int i = 5;
        int h = 10 * i++; // 10 x 5 (stays the same for now) -> if it was on the left side
        // "++i" i would add 1-> 10 x 6=60

        System.out.println("\n.....Task6......\n");

        int var1 = 27;
        int result = --var1  /2;

        System.out.println(++result); // 14














































    }
}
