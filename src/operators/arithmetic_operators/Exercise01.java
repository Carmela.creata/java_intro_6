package operators.arithmetic_operators;

public class Exercise01 {
    public static void main(String[] args) {
        int a = 5;
        int b = 8;
        int area = a * b;
        int perimeter = 2*(a + b);

        System.out.println("Area = " +  a * b );
        System.out.println("Perimeter = " + 2 * (a + b));
    }
}
