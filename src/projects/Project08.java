package projects;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {
        System.out.println("***** TASK 1 *****");
        System.out.println(findClosestDistance(new int[]{4}));
        System.out.println(findClosestDistance(new int[]{4, 8, 7, 15}));
        System.out.println(findClosestDistance(new int[]{10, -5, 20, 50, 100}));

        System.out.println("\n***** TASK 2a *****");
        System.out.println(findSingleNumber(new int[]{2}));
        System.out.println(findSingleNumber(new int[]{5, 3, -1, 3, 5, 7, -1}));
        System.out.println(findSingleNumber(new int[]{5, 3, -1, 3, 5, 5, 7, 3, -1}));


        System.out.println("\n***** TASK 3 *****");
        System.out.println(findFirstUniqueCharacter("Hello"));
        System.out.println(findFirstUniqueCharacter("abc abc d"));
        System.out.println(findFirstUniqueCharacter("abab"));

        System.out.println("\n***** TASK 4 *****");
        System.out.println(findMissingNumber(new int[]{2, 4}));
        System.out.println(findMissingNumber(new int[]{2, 3, 1, 5}));
        System.out.println(findMissingNumber(new int[]{4, 7, 8, 6}));
    }




    public static int findClosestDistance(int[] nums) {
        //sort the array
        //check the length -> -1 if length < 2
        //compare the numbers one by one

        Arrays.sort(nums);//sort them in ascending order
        if (nums.length < 2) return -1;

        int difference = Integer.MAX_VALUE; //the max value of the smallest difference possible

        for (int i = 0; i + 1 < nums.length ; i++) {  //next element - previous element = my difference
            int currentDiff = nums[i + 1] - nums[i]; // nums[i+1] is the next element & is the biggest index that I m using
            //find the min difference: min between current diff & max value of my initial diff variable
            difference = Math.min(currentDiff, difference);


        }
        return difference;

    }

    public static int findSingleNumber(int[] numbers) {
        //find the element that occurs once
        //check the length = 1; return the arr[0]
        //then loop thru it and find if the element is not equal with previous element

        Arrays.sort(numbers); //sort it

        if (numbers.length == 1) return numbers[0];

        int i = 0;

        while (i < numbers.length - 1) { // if nr at index 0 is not equal with the next, return the nr
            if (numbers[i] != numbers[i + 1]) return numbers[i];

            i = i + 2; //1122334456 -> checks the nrs 2 by 2 until it reaches the lonely one


        }
        return numbers[i];

    }

    public static String  findFirstUniqueCharacter(String str){
        // we can ignore spaces
        str = str.replaceAll(" ", "");

        // create int array with all possible ascii values
        int[] charCount = new int[128]; // [][][][][][][][][][][][][].....[][][][H][][][h]

        // first loop will build the int array
        for (int i = 0; i < str.length(); i++) {
            charCount[str.charAt(i)]++;
        }

        // second loop will iterate through the int array
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (charCount[c] == 1) return "" + c;
        }
        return "";


    }

    public static int  findMissingNumber(int [] arr){
        // Sort the sequence to compare the current value with the next
        // if the number differs by 2, then the missing number is current number + 1
        Arrays.sort(arr);

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] + 1 != arr[i + 1]) return arr[i] + 1;
        }
        return 0;


    }



}