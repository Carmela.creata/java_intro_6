package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n......TASK 1......\n");

        String name = "Carmela";
        System.out.println("My name is " + name);

        System.out.println("\n......TASK 2......\n");

        char  nameCharacter1 = 'c';
        char  nameCharacter2 = 'a';
        char  nameCharacter3 = 'r';
        char  nameCharacter4 = 'm';
        char  nameCharacter5 = 'e';
        char  nameCharacter6 = 'l';
        char  nameCharacter7 = 'a';
        System.out.println("My name 1 letter is = " + nameCharacter1);
        System.out.println("My name 2 letter is = " + nameCharacter2);
        System.out.println("My name 3 letter is = " + nameCharacter3);
        System.out.println("My name 4 letter is = " + nameCharacter4);
        System.out.println("My name 5 letter is = " + nameCharacter5);
        System.out.println("My name 6 letter is = " + nameCharacter6);
        System.out.println("My name 7 letter is = " + nameCharacter7);


        System.out.println("\n......TASK 3......\n");

        String myFavMovie = "Pride and Prejudice";
        String myFavSong = "You don't own me";
        String myFavCity = "Bucharest";
        String myFavActivity = "yoga";
        String myFavSnack = "rice pudding";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite song is = " +  myFavSong);
        System.out.println("My favorite city is = " +  myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite snack is = " + myFavSnack);


        System.out.println("\n......TASK 4......\n");


        int myFavNumber = 5;
        int numberOfStatesIVisited = 32;
        int numberOfCountriesIVisited = 3;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("My number of states I visited is = " + numberOfStatesIVisited);
        System.out.println("My number of countries I visited is = " + numberOfCountriesIVisited);

        System.out.println("\n......TASK 5......\n");

        boolean amIAtSchoolToday = false;

        System.out.println("I am at school today = " + amIAtSchoolToday);

        System.out.println("\n......TASK 6......\n");

        boolean isWeatherNiceToday = false;
        System.out.println("The weather is nice today = " + isWeatherNiceToday);





















































    }
}


