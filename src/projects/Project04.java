package projects;

import utilities.ScannerHelper;


public class Project04 {
    public static void main(String[] args) {

        System.out.println("\n***** TASK 1 *****\n");

        String str = ScannerHelper.getString();

        if (str.length() < 8) System.out.println("This String does not have 8 characters");
        else
            System.out.println(str.substring(str.length() - 4) + str.substring(4, str.length() - 4) + str.substring(0, 4));


        System.out.println("\n***** TASK 2 *****\n");

        String sentence = ScannerHelper.getSentence();

        if(sentence.contains(" ")) System.out.println(sentence.substring(sentence.lastIndexOf(' ')+1) +
                sentence.substring(sentence.indexOf(' '),sentence.lastIndexOf(' ')) + " " +  sentence.substring(0,sentence.indexOf(' ')));
        else System.out.println("This sentence does not have 2 or more words to swap");





        System.out.println("\n***** TASK 3 *****\n");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid t-shirts in the past and also some idiot look shoes";


        System.out.println(str1.replace("stupid", "nice"));
        System.out.println(str2.replace("idiot", "nice"));
        System.out.println(str3.replace("stupid", "nice").replace("idiot", "nice"));


        System.out.println("\n***** TASK 4 *****\n");

        String name = ScannerHelper.getName();

        if (name.length() < 2) System.out.println("Invalid input!!!");
        else if (name.length() % 2 == 1) System.out.println(name.charAt((name.length() - 1) / 2));
        else System.out.println(name.substring(name.length() / 2 - 1, name.length() / 2 + 1));


        System.out.println("\n***** TASK 5 *****\n");

        String favCountry = ScannerHelper.getFavCountry();   //I already had favCountry method in Scanner

        if (favCountry.length() < 5) System.out.println("Invalid input!!!");
        else System.out.println(favCountry.substring(2, favCountry.length() - 2));


        System.out.println("\n***** TASK 6 *****\n");

        String address = ScannerHelper.getAddress();

        System.out.println(address.replace("a", "*").replace("e", "#").replace("i", "+").replace("I", "+")
                .replace("u", "$").replace("o", "@"));


        System.out.println("\n***** TASK 7 *****\n");

        String sentence2 = ScannerHelper.getSentence();

        String[] word = sentence2.split(" ");


        if (sentence2.length() < 2) System.out.println("This sentence does not have multiple words");
        else System.out.println("This sentence has " + word.length + " words");


    }
}
