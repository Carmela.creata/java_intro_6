package projects;

import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {

        int[] task1 = {10, 7, 7, 10, -3, 10, -3};
        greatestAndSmallestWithSort(task1);
        int[] task2 = {10, 7, 7, 10, -3, 10, -3};
        greatestAndSmallestWithoutSort(task2);
        int[] task3 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(task3);
        int[] task4 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallest(task4);
        String[] task5 = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(task5);
        String [] task6 ={"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(task6);


    }

    public static void greatestAndSmallestWithSort(int[] arr){

        if(arr.length > 0){
        Arrays.sort(arr);
        System.out.println("Smallest = " + arr[0]);
        System.out.println("Greatest = " + arr[arr.length -1]);}
        else System.out.println("The array contains no elements");
    }


    public static void greatestAndSmallestWithoutSort(int[] arr){


        System.out.println(Arrays.toString(arr));

        int smallest = arr[0];
        int largest = arr[0];

        for(int array : arr){
            if(array < largest) largest = array;
            else if(array > smallest) smallest =array;
        }
        System.out.println("Smallest = " + largest);
        System.out.println("Greatest = " + smallest);

    }

    public static void  findSecondGreatestAndSmallestWithSort(int [] arr){

        if(arr.length > 0){
        Arrays.sort(arr);
        int min = arr[0];
        int max = arr[arr.length -1];

        int secondMin = 0;
        int secondMax = 0;

            for (int i = 1; i < arr.length -1 ; i++) {
                if (arr[i] != min){ secondMin = arr[i];
                    break;}
            }
            for (int i = arr.length - 1; i > 0; i--) {
                if(arr[1] != max){ secondMax = arr[1];
                    break;

            }


                System.out.println("Second smallest = " + secondMin);
                System.out.println("Second greatest = " + secondMax);
            }
    }
    }


        public static void findSecondGreatestAndSmallest(int[] arr) {
            int n = arr.length;
            int max1 = arr[0], max2 = Integer.MIN_VALUE;
            int min1 = arr[0], min2 = Integer.MAX_VALUE;


            for (int i = 1; i < n; i++) {
                if (arr[i] > max1) {
                    max2 = max1;
                    max1 = arr[i];
                } else if (arr[i] > max2 && arr[i] != max1) {
                    max2 = arr[i];
                }

                if (arr[i] < min1) {
                    min2 = min1;
                    min1 = arr[i];
                } else if (arr[i] < min2 && arr[i] != min1) {
                    min2 = arr[i];
                }
            }

            System.out.println("Second greatest element is: " + max2);
            System.out.println("Second smallest element is: " + min2);
        }




    public static void findDuplicatedElementsInAnArray(String[] array){


        System.out.println(Arrays.toString(array));

        boolean duplicate = false;

        for (int i = 0; i < array.length -1; i++) {

            for (int j = i + 1; j < array.length ; j++) {
                if(array[i].equals(array[j]))
                {
                    System.out.println(array[i]);
                    duplicate = true;
                }
            }
        }

    }

    public static void  findMostRepeatedElementInAnArray(String[] array){


        System.out.println(Arrays.toString(array));

        String mostRepeated = "";
        int count = 0;
        int maxCount = 0;

        for(String element1 : array) {
            count = 0;

            for (String element2 : array) {
                if (element1.equals(element2)) count++;
            }
            if (count > maxCount) {
                maxCount = count;
                mostRepeated = element1;

            }
        }
            System.out.println(mostRepeated);

    }


}
