package projects;

import java.util.ArrayList;

public class Project07 {
    public static void main(String[] args) {
        String[] task1 = {"foo", "", " ", "foo bar", "java is fun", " ruby "};
        System.out.println(countMultipleWords(task1));

      Integer[] task2 = {2, -5, 6, 7, -10, -78, 0, 15};
        System.out.println(removeNegatives(task2));

        System.out.println(validatePassword("Abcd123!"));


    }

    /*
    TASK-1 - countMultipleWords() method
•Write a method that takes a String[] array as an argument and counts
how many strings in the array has multiple words.
•This method will return an int which is the count of elements that have
multiple words.
•NOTE: be careful about these as they are not multiple words ->“”,    “   “,
“    abc”,  “abc   “
Test data:
[“foo”, “”, “ “, “foo bar”, “java is fun”, “ ruby ”]
Expected output:
2
     */
    public static int countMultipleWords(String [] arr) {
        int count = 0;
        for (String str : arr) {
            if (str.trim().split(" ").length > 1) {
                count++;
            }

        }
        return count++;
    }
    /*
    TASK-2 - removeNegatives() method
•Write a method that takes an “ArrayList<Integer> numbers” as an
argument and removes all negative numbers from the given list if there
are any.
•This method will return an ArrayList with removed negatives.
Test data 1:
[2, -5, 6, 7, -10, -78, 0, 15]
Expected output 1:
[2, 6, 7, 0, 15]
     */
    public static ArrayList<Integer> removeNegatives(Integer[] numbers){
        ArrayList<Integer> result = new ArrayList<>();

        for (int num : numbers) {
            if (num >= 0) {
                result.add(num);
            }
        }

        return result;
    }

    /*
    TASK-3 - validatePassword() method
•Write a method that takes a “String password” as an argument and
checks if the given password is valid or not.
•This method will return true if given password is valid, or false if given
password is not valid.
•A VALID PASSWORD:
-should have length of 8 to 16 (both inclusive).
-should have at least 1 digit, 1 uppercase, 1 lowercase and 1 special
char.
-should NOT have any space.
Test data 1:
Expected output 1:false Test data 2:abcdExpected output 2:false
Test data 3:abcd1234 Expected output 3:false
Test data 5:Abcd123!Expected output 5:true

     */
    public static boolean validatePassword(String password) {

        if (password.length() < 8 || password.length() > 16) {
            return false;
        }
        boolean hasDigit = false;
        boolean hasUpperCase = false;
        boolean hasLowerCase = false;
        boolean hasSpecialChar = false;

        for (int i = 0; i < password.length(); i++) {
            char c = password.charAt(i);

            if (Character.isDigit(c)) {
                hasDigit = true;
            } else if (Character.isUpperCase(c)) {
                hasUpperCase = true;
            } else if (Character.isLowerCase(c)) {
                hasLowerCase = true;
            } else if (!Character.isWhitespace(c)) {
                hasSpecialChar = true;
            }
        }

        if (!hasDigit || !hasUpperCase || !hasLowerCase || !hasSpecialChar) {
            return false;
        }


        return !password.contains(" ");
    }

    /*
    TASK-4 - validateEmailAddress() method
•Write a method that takes a “String email” as an argument and checks if
the given email is valid or not.
•This method will return true if given email is true, or false if given email is
not valid.
•A VALID EMAIL:
-should NOT have any space.
-should not have more than one “@” character.
-should be in the given format <2+chars>@<2+chars>.<2+chars>
Test data 1: a@gmail.com Expected output 1: false
Test data 2: abc@g.com Expected output 2: false
Test data 3:abc@gmail.c Expected output 3:false
Test data 4: abc@@gmail.com Expected output 4: false
Test data 5: abcd@gmail.com
Expected output 5:true
     */
    /*
    public static boolean validateEmailAddress(String email){
        if (email.contains(" ")) {
            return false;
        }

        if (email.indexOf("@") != email.lastIndexOf("@")) {
            return false;
        }


    }
    */








    }











