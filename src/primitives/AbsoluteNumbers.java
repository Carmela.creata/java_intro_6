package primitives;

public class AbsoluteNumbers {
    public static void main(String[] args) {
        byte myNumber = 45;
        System.out.println(myNumber);

        System.out.println("\n....byte...\n");

        System.out.println("The max value of byte = " + Byte.MAX_VALUE);
        System.out.println("The max value of byte = " + Byte.MIN_VALUE);

        System.out.println("\n....short...\n");
       short numberShort = 150;
        System.out.println(numberShort);
        System.out.println("The max value of short = " + Short.MAX_VALUE);
        System.out.println("The min value of short = " + Short.MIN_VALUE);


        System.out.println("\n....int....\n");
        int myInteger = 2000000;

        System.out.println(myInteger);
        System.out.println("The max value of int = " + Integer.MAX_VALUE); //2147483647
        System.out.println("The min value of int = " + Integer.MIN_VALUE); //-2147483647


        System.out.println("\n.....long....\n");
        long myBigNumber = 2147483648L;
        System.out.println(myBigNumber); // 2147483648

        System.out.println("\n.....long more info....\n");
        long l1 = 5;
        long l2 = 2354759503726L;
        System.out.println(l1);
        System.out.println(l2);
























    }
}
