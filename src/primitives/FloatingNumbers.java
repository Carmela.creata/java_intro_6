package primitives;

public class FloatingNumbers {
    public static void main(String[] args) {

        System.out.println("\n.....floating numbers....\n");
        float myFloat1 = 20.5F;
        double myDouble1 = 20.5;
        System.out.println(myFloat1);
        System.out.println(myDouble1);

        float myFloat2 = 10;
        double myDouble2 = 234657;

        System.out.println(myFloat2);
        System.out.println(myDouble2);


        float f1 = 32746237.23423542352352F;
        double d1 = 3274.23423542352352;

        System.out.println(f1);
        System.out.println(d1);












    }
}
