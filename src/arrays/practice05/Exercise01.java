package arrays.practice05;

public class Exercise01 {
    public static void main(String[] args) {
        getFirstPosAndNeg();
    }

    public static void getFirstPosAndNeg(){

        /*
        Write a program to find the first positive and negative
    numbers in an int array
    Test data:
    int[] numbers = {0, -4, -7, 0, 5, 10, 45};
    Expected output:
    First positive number is: 5
    First negative number is: -4
    NOTE: Make your code dynamic that works for any
    given int array.
    React
    Reply

         */

        int[] numbers = {0, -4, -7, 0, 5, 10, 45};


        //1st way
        int firstPos = 0, firstNeg = 0;

        for(int n : numbers) {
            if (n > 0) {
                firstPos = n;
                break;
            }
        }

        for (int n : numbers){
            if(n < 0){
                firstNeg = n;
                break;
            }
        }

        System.out.println("First positive number is: " + firstPos);
        System.out.println("First negative number is: " + firstNeg);

        //2nd way
        boolean isEvenFound = false; // -> starts false because it tells us to find the nr
        boolean isOddFound = false; // -> then it will run true or false if it finds the number

        for (int number : numbers) {

            if(!isEvenFound && number % 2 == 0){
                System.out.println( "First positive number is: " + number);
                isEvenFound = true;
            }
            else if(!isOddFound && number % 2 == 1){
                System.out.println("First negative number is: " + number);
                isOddFound = true;
            }

            if(isEvenFound && isOddFound) break; // When both are found, break the loop
        }

    }

}