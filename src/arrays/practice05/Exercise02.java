package arrays.practice05;

public class Exercise02 {
    public static void main(String[] args) {
        String[] arr = {"red", "blue", "yellow", "white"}; // create the array in the main
        getShortestLongest(arr);//declare the method and put in your array - it returns an array
    }
    public  static void getShortestLongest(String[] array){
        /*
        Requirement:
    Write a program to find the longest and shortest strings
    in an String array
    Test data:
    String[] words = {"red", "blue", "yellow", "white"};
    Expected output:
    The longest word is = yellow
    The shortest word is = red
    NOTE: Make your code dynamic that works for any
    given String array.
         */

        String[] words = {"red", "blue", "yellow", "white"};

        String longest  = words[0]; // words were declared in the method, so you put it as a count
        String shortest = words[0];

        for (String s : words) { ///// for every word in the string
            if(s.length() > longest.length()) longest = s; // if each word length bigger than the count length then
            else if(s.length() < shortest.length()) shortest = s; //-> the 's'(every word) is equal with the count
        }
        System.out.println("The longest word is = " + longest);
        System.out.println("The shortest word is = " + shortest);

    }

}






