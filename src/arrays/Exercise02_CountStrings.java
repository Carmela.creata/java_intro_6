package arrays;

import java.util.Arrays;

public class Exercise02_CountStrings {
    public static void main(String[] args) {
        //declare a string array called as countries and assign size of 3
        String[] countries = new String[3];

        //assign "Spain" to index of 1
        countries[1] = "Spain";

        //print the value at index of 1 and 2
        System.out.println(countries[1]);
        System.out.println(countries[2]);

        //assign Belgium at index of 0 and Italy at index of 2
        countries[0] = "Belgium";
        countries[2] = "Italy";

        //print the arrays
        System.out.println(Arrays.toString(countries));

        //sort this array then print it again
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries));


        // print each element with for i loop
        for (int i = 0; i < countries.length; i++) {
            System.out.println(countries[i]);

        }
        //print each element with for each loop (iter shortcut --> use)
        for (String country:countries){
                  System.out.println(country);
        }

        //count how many country has 5 characters ->
       int count = 0;
        for (String country : countries) {
            if(country.length() == 5 ) count++;
        }

        System.out.println(count);

        //count how many countries have letter I or i in their name ->

        int countI = 0;

        for (String country : countries){
              if (country.toLowerCase().contains("i")) {
                  countI++;
              }

        }
        System.out.println(countI);



    }
}
