package arrays;

import java.lang.reflect.Array;
import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {

        // 1. Create an array to store -> 5.5, 6, 10.3, 25
        double[] numbers = {5.5, 6.0, 10.3, 25.0};


        // 2. Print the array
        System.out.println(Arrays.toString(numbers));

        // 3. Print the size of the array
        System.out.println("The length is " + numbers.length);

        // 4. Print each element using for each loop
        for(double numberArray : numbers){
            System.out.println(numberArray);
        }
    }
}
