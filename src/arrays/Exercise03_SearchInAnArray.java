package arrays;

import java.util.Arrays;

public class Exercise03_SearchInAnArray {
    public static void main(String[] args) {

        String[] objects = {"Remote", "Mouse", "Mouse", "Keyboard", "iPad"};

    /*
    Check the collection you have above and print true if it contains Mouse
    Print false otherwise

    RESULT:
    true
    */
        Arrays.sort(objects);
        System.out.println(Arrays.binarySearch(objects, "mouse") >= 0);


        boolean mouse = false;

        for (String object : objects) {
            if (object.equals("mouse")) {
                mouse = true;
                break;
            }
        }
        System.out.println(mouse);
    }
}
