package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {

        //how many character does the String has

        String str = ScannerHelper.getString();

        int countL = 0;

        for (int i = 0; i < str.length(); i++) {
            if(Character.isLetter(str.charAt(i))) countL++;

        }
        System.out.println(countL);

        //second way

       countL = 0;

        for(char c :ScannerHelper.getString().toCharArray() ){
            if(Character.isLetter(c)) countL++;
        }
        System.out.println(countL);



    }
}
