package escape_sequences;

public class Exercise01 {
    public static void main(String[] args) {
      // System.out.println("\tToday, I woke up at 7:00 AM.\nThen, I went to the gym and did my daily exercise.\nLater, after having breakfast, I will need to study.");


       System.out.println("\tJava is a high-level, class-based, " +
               "\nobject-oriented programming language that is " +
               "\ndesigned to have as few implementation dependencies\n " +
               "as possible.\n\n\tJava was originally developed by\nJames Gosling at Sun Microsystems. " +
               "It was released\n in May 1995 as a core component of Sun Microsystems' Java platform. " +
               "\n\n\tAs of March 2022, Java 18 is the latest version, while\n " +
               "Java 17, 11 and 8 are the current long-term support (LTS)\n versions.");




    }
}
