package practice;

import utilities.ScannerHelper;

public class Exercise_06StringMethods {
    public static void main(String[] args) {
         /*
        Write a program that asks user to enter a String.

        The program checks if a given String has length of 4 at least and starts and ends with xx.
        -If the length of String is less than 4, then print "INVALID INPUT"
        -If given String starts and ends with xx, then print true.
        -Otherwise, print false

        ""          -> INVALID INPUT
        "red"       -> INVALID INPUT
        "java"      -> false
        "xxbluexx"  -> true

         */

        String str = ScannerHelper.getString();

        if(str.length() < 4) System.out.println("INVALID INPUT");
        System.out.println(str.length() >= 4 && str.startsWith("xx") && str.endsWith("xx"));
    }
}
