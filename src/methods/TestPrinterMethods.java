package methods;

import utilities.MathHelper;
import utilities.Printer;

public class TestPrinterMethods {
    public static void main(String[] args) {

        Printer.printGM();
        Printer.printGM();

        Printer.helloName("Jazzy");
        Printer.helloName("Carmela");

    //static vs non-static
    // static can be invoked with the class name
    //non-static can be invoked with an object of the class

        Printer myPrinter = new Printer();
        myPrinter.printTechGlobal();
        // we created an object (like Scanner) to be
        //able to show up when we run the code.
        //it won't show up. - it is non-static so create an object
        //all the time

        MathHelper.sum(3,5); // 8
        Printer.printGM();// it does not return anything


    }

}
