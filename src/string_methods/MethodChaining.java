package string_methods;

public class MethodChaining {
    public static void main(String[] args) {


            String str = "TechGlobal";

            // single method
            System.out.println(str.toLowerCase()); // techglobal

            // 2 methods chained
            System.out.println(str.toLowerCase().contains("tech")); // true

            // 3 method chained
            System.out.println(str.toUpperCase().substring(4).length()); //TechGlobal -> TECHGLOBAL -> Global -> 5

            // 6 method chained
            String sentence = "Hello, my name is John Doe. I am 30 years old and I go to school at TechGlobal";
            System.out.println(sentence.toLowerCase().replace("a", "X")
                    .replace("e", "X")
                    .replace("i", "X")
                    .replace("o", "X")
                    .replace("u", "X").charAt(sentence.length()-1));



    }
}
