package string_methods;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {

        /*
        Write a Java program that asks user to enter their favorite
        book name and quote
        And store answers of user in different Strings
        Finally, print the length of those Strings with proper
        messages

         */

        String favoriteBook = ScannerHelper.getFavoriteBook();
        String favoriteQuote = ScannerHelper.getFavoriteQuote();

        System.out.println("The length of your favorite book is " + favoriteBook.length());
        System.out.println("The length of your favorite book is " + favoriteQuote.length());
    }
}
