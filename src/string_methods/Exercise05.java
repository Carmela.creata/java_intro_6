package string_methods;

import utilities.ScannerHelper;

public class Exercise05 {
    public static void main(String[] args) {

        String word = ScannerHelper.getString().toLowerCase();


        System.out.println(word.startsWith("a") && word.endsWith("e"));


    }
}
