package string_methods;

public class Exercise03 {
    public static void main(String[] args) {

        String sent = "The best is Java";

        String java = sent.substring(12);
        System.out.println(java);


        //second way

        java = sent.substring(sent.indexOf("Java"));
        System.out.println(java);

        //third way
        java = sent.substring(sent.lastIndexOf(' ')).trim();
        System.out.println(java);

    }
}
