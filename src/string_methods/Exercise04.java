package string_methods;

public class Exercise04 {
    public static void main(String[] args) {
        /*
        assume you have a string "I go to TechGlobal
        extract every word from the string into other strings
        and print them out on different lines
        ex:
         */
        String text = "I go to TechGlobal";

        System.out.println(text.charAt(0));
        System.out.println(text.substring(2,4));
        System.out.println(text.substring(5,7));
        System.out.println(text.substring(8));
    }
}
