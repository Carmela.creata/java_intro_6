package string_methods;

import utilities.ScannerHelper;

public class _13_ContainsMethod {
    public static void main(String[] args) {
        /*
        1.return type
        2.returns boolean
        3.non-static
        4.takes a str as an argument
         */

        String name = "John Doe";

        boolean containsJohn = name.toLowerCase().contains("john");
        System.out.println(containsJohn);

        //write a program to ask user for a string
        //return true if this string is a sentence and false if it is a single word
        // use ScannerHelper
        //check if string contains spaces
        // and if it ends with period

        String str = ScannerHelper.getString();

        System.out.println(str.contains(" ") && str.endsWith("."));



    }
}
