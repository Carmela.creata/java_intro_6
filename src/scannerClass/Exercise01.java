package scannerClass;

import java.util.Scanner;

public class Exercise01 {
    public static void main(String[] args) {
        Scanner inputReader = new Scanner(System.in);
        System.out.println("Please enter your first name");
        String fName = inputReader.next();
        inputReader.nextLine(); //empty line to skip the problem

        System.out.println("Please enter your address");
        String address = inputReader.nextLine();


        System.out.println("Please enter your favNumber");
     int favNumber= inputReader.nextInt();

        System.out.println(fName +"'s address is " +  address + " and their address is " +  favNumber + ".");


    }
}
