package scannerClass;

import java.sql.SQLOutput;
import java.util.Scanner;

public class FirstScannerProgram {
    public static void main(String[] args) {

        // next method
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your name.");
        String name = input.next();
        input.nextLine();

        System.out.println("The user's name is: " + name);
        System.out.println("\n..................\n");

        // nextLine method

        System.out.println("Please enter your first and last name");
        String fullName = input.nextLine();
        System.out.println("The users full name is:" + fullName);


        //nextInt method
        System.out.println("Please enter a number");
        int number = input.nextInt();

        System.out.println("The number you chose is: " + number);




























    }
}
